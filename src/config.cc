/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "adk/config/config.h"
#include <stdlib.h>
#include <map>
#include <memory>
#include "adk_cfg_db_manager.h"

#include <assert.h>

namespace adk {
namespace config {

struct Config::Private {
  explicit Private(const std::string &filename) : qsap_db_filename(filename) {}
  /**
    *  @variable config_manager_
    *  @brief ConfigManager object that handles all sqlite calls.
    */
  ConfigManager config_manager;
  /**
    *  @variable qsapdbFilename_
    *  @brief filename containing the path to the configuration database.
    */
  const std::string qsap_db_filename;
};
Config::~Config() = default;

Config::Config(const std::string &dbFilename)
    : self_(new Private(dbFilename)) {}

bool Config::Read(bool *value, const std::string &key) const {
  std::string string_value;
  if (self_->config_manager.ReadConfig(&string_value, key)) {
    // Account for boolean value being "1", "true", "TRUE" etc..
    return CoerceToBool(string_value, value);
  }
  return false;
}

bool Config::Read(bool *value, int index, const std::string &key,
                  const std::string &subkey) const {
  std::string string_value;
  // Read the key.__INDEX__.subkey allowing for indexed keys.
  if (self_->config_manager.ReadConfig(
          &string_value, key + "." + std::to_string(index) + "." + subkey)) {
    // Account for boolean value being "1", "true", "TRUE" etc..
    return CoerceToBool(string_value, value);
  }
  return false;
}

bool Config::Read(int32_t *value, const std::string &key) const {
  std::string input;
  if (self_->config_manager.ReadConfig(&input, key)) {
    // Attempt to convert the string to an int. return false if not possible
    try {
      *value = std::stoi(input);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
  } else {
    return false;
  }
  return true;
}

bool Config::Read(int32_t *value, int index, const std::string &key,
                  const std::string &subkey) const {
  std::string input;
  // Read the key.__INDEX__.subkey allowing for indexed keys.
  if (self_->config_manager.ReadConfig(
          &input, key + "." + std::to_string(index) + "." + subkey)) {
    // Attempt to convert the string to an int. return false if not possible
    try {
      *value = std::stoi(input);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
  } else {
    return false;
  }
  return true;
}

bool Config::Read(double *value, const std::string &key) const {
  std::string read_string;
  // Read the key.__INDEX__.subkey allowing for indexed keys.
  if (self_->config_manager.ReadConfig(&read_string, key)) {
    try {
      // Attempt to convert the string to an double. return false if not
      // possible
      *value = std::stod(read_string);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
    return true;
  }
  return false;
}

bool Config::Read(double *value, int index, const std::string &key,
                  const std::string &subkey) const {
  std::string read_string;

  if (self_->config_manager.ReadConfig(
          &read_string, key + "." + std::to_string(index) + "." + subkey)) {
    // Attempt to convert the string to an double. return false if not
    // possible
    try {
      *value = std::stod(read_string);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
    return true;
  }
  return false;
}

bool Config::Read(std::string *value, const std::string &key) const {
  // Read the value straight into the passed pointer
  return self_->config_manager.ReadConfig(value, key);
}

bool Config::Read(std::string *value, int index, const std::string &key,
                  const std::string &subkey) const {
  // Read the key.__INDEX__.subkey allowing for indexed keys.
  return self_->config_manager.ReadConfig(
      value, key + "." + std::to_string(index) + "." + subkey);
}

bool Config::Write(bool value, const std::string &key, bool create) {
  // Booleans written using the config library will be stored as "0" and "1"
  int writevalue = 0;
  if (value) {
    writevalue = 1;
  } else {
    writevalue = 0;
  }
  return self_->config_manager.WriteConfig(key, std::to_string(writevalue),
                                           true, create);
}

bool Config::Write(int32_t value, const std::string &key, bool create) {
  return self_->config_manager.WriteConfig(key, std::to_string(value), true,
                                           create);
}

bool Config::Write(const char *value, const std::string &key, bool create) {
  return self_->config_manager.WriteConfig(key, value, true, create);
}

bool Config::Write(double value, const std::string &key, bool create) {
  return self_->config_manager.WriteConfig(key, std::to_string(value), true,
                                           create);
}

bool Config::Write(const std::string &value, const std::string &key,
                   bool create) {
  // Convert the std::string to a c-style string and call the c-string method.
  return Write(value.c_str(), key, create);
}

bool Config::Restore(const std::string &key) {
  return self_->config_manager.RestoreDefault(key);
}

bool Config::KeyExists(const std::string &key) {
  return self_->config_manager.KeyExists(key);
}

bool Config::InitialiseManager() {
  // If there is no filepath infomation then prepend default filepath
  std::string config_database_path;
  if (self_->qsap_db_filename.find('/') == std::string::npos) {
    config_database_path = CONFIG_PATH + self_->qsap_db_filename;
  } else {
    config_database_path = self_->qsap_db_filename;
  }

  return self_->config_manager.InitialiseManager(config_database_path);
}

std::unique_ptr<Config> Config::Create(const std::string &database_file) {
  std::unique_ptr<Config> factory_config(new Config(database_file));
  // If the initialisation failed then return a nullptr object.
  if (!factory_config->InitialiseManager()) {
    return {};
  }
  return factory_config;
}

}  // namespace config
}  // namespace adk
