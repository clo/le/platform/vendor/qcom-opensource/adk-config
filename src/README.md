# adkcfg

 ADK Linux Config Tool
=============================


Requirements
------------

* Git
* CMake 3.1 or higher
* A C++ compiler, e.g., GCC,
* SQLITE3 Installed 
* JSON-C (libjson0-dev libjson0) Installed

How to build
-----------
```
sudo apt-get install libsqlite3-dev libjson-c-dev sqlite3
```

```
mkdir build
cd build
cmake ../
make
```

Usage
=============================

This is a linux style command line tool that should enable the modification and reading
of the sqlite database which will be used to store the configuration data on the ADK platform

The project contains the adkcfgmgr class which contains all the methods and the connection
to the sqlite database

**libsqlite3-dev** should be installed as the application does not compile the sqlite3 library.

`adkcfg --help`

for help
`adkcfg --version`

for application version

read
-----------

`adkcfg read KEY`

read a value for the given KEY

`adkcfg read KEY -l`

read all values with key **LIKE** KEY

`adkcfg read KEY -a` 

Show verbose information on given KEY

list
-----------
`adkcfg list`

list all key, value pairs in the database.

`adkcfg list -a`

Show verbose information in list. 

write
-----------
`adkcfg write KEY VALUE`

write the given VALUE to KEY. VALUE will be checked against constraints.
Will fail if KEY is set to read_only.

`adkcfg write KEY VALUE --ignore`

write the VALUE to KEY but ignore any constraints or read_only flags

add
-----------
`adkcfg add KEY VALUE`

add a new key, value pair to the configuration. Will assume that the default value is the input value
and the meta information will be set to "uknown" at meta_id 1

`adkcfg add KEY VALUE --default D_VALUE`

add a new key, value pair to the configuration but with a specified default value.

`adkcfg add KEY VALUE --meta META_ID`

add a new key, value pair to the configuration but with the given meta table link. If the input VALUE does not fulfill
the constraints then the add will fail. 

delete
-----------
`adkcfg delete KEY` 

Deletes the key, value pair. The constraints will remain as they could be used by another key.

`adkcfg delete KEY -a`

Deletes the key, value pair. will attempt to delete the constraints aswell. however if it used by another key then it wont be deleted.

reset 
-----------

`adkcfg reset KEY` 

reset a key to it's stored default value

`adkcfg reset -a`

reset all values in configuration to their default value

meta
-----------
`adkcfg meta VALUE_TYPE VALUE_CONSTRAINTS DESCRIPTION`

Add a new meta entry. If succesuful will return the new ID. Should be used before `adkcfg add` to specify meta for new key, value pair
VALUE_CONSTRAINTS must be written in correct JSON format. An error will be shown if JSON is not parceable.
`adkcfg meta --list`

list all entries in meta table

`adkcfg meta --delete META_ID`

delete a meta entry with the given META_ID

`adkcfg meta --key KEY`

read the meta information of a KEY

`adkcfg meta --modify META_ID VALUE_CONSTRAINTS`

write new meta constraints for meta at id META_ID

config
-----------

`adkcfg config --version`

gives the version of the database stored in database_meta_table.

Constraint JSON format
=============================
The constraints in the meta information is stored using JSON format.
There are three types of constraint. `range`, `list` and `enum` a constraint can constain multiple ranges. i.e 1-10 and 30-50
A range **shall** have a min and a max key.

example list json format with strings:

`{"list":["string1","string2","string3"]}`

example list with ints:

`{"list":[1,2,3]}`

example single range:

`{"range":[{"min":1,"max":10}]}`

example multi range:

`{"range":[{"min":1,"max":10},{"min":30,"max":50}]}`


example multi range:

`{"enum":{"FAILURE":1,"WARNING":2,"SUCCESS":3}}`

TODO
=============================
Enums currently crudely implemented. Stored in value as an int. When reading the value ideally it should return real name rather than integer value.


Example Database
=============================

To generate an example database for this module to test with use the following sqlite commands.
One way of generating sqlite database is to put these instructions into a txt file and run the following:
`cat schema.txt | sqlite3 adk.core.db`

```
pragma foreign_keys = ON;
drop table database_meta_table;
drop table config_table;
drop table meta_table;

CREATE TABLE database_meta_table(key TEXT, value TEXT);

CREATE TABLE config_table(key TEXT UNIQUE, value TEXT, value_default TEXT, meta_id INTEGER,read_only INTEGER,internal_only INTEGER,FOREIGN KEY(meta_id) REFERENCES meta_table(id));
                        
CREATE TABLE meta_table( id INTEGER PRIMARY KEY, description TEXT, value_type TEXT,  value_constraints TEXT);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("UKNOWN",NULL,"Default constraints for UKNOWN types");

INSERT INTO database_meta_table(key,value) VALUES ("database_version","1.0");
INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("int",'{"list":[1,2,5,6]}',"A GPIO mapping of a button");
INSERT INTO config_table VALUES ("ui.button.1.gpio",1,5,2,0,0);
INSERT INTO config_table VALUES ("ui.button.2.gpio",2,5,2,1,0);
INSERT INTO config_table VALUES ("ui.button.3.gpio",6,5,2,0,1);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("str",'{"list":["up","down","left","right"]}',"A ui element orientation");
INSERT INTO config_table VALUES ("ui.direction","up","down",3,0,0);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("int",'{"range":[{"min":1,"max":20}]}',"the clock frequency in MHz");
INSERT INTO config_table VALUES ("system.clock",2,15,4,1,1);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("double",'{"range":[{"min":3.3,"max":5.0}]}',"the system operating voltage");
INSERT INTO config_table VALUES ("system.voltage",4.2,4.5,5,1,0);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("int",'{"range":[{"min":3,"max":5},{"min":7,"max":10}]}',"the system operating voltage");
INSERT INTO config_table VALUES ("pwr.gpio",4,5,6,1,0);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("double",'{"range":[{"min":2.4,"max":5.0},{"min":7.6,"max":10.1}]}',"the system operating voltage");
INSERT INTO config_table VALUES ("regulator.voltage",2.6,4.7,7,1,0);

INSERT INTO meta_table(value_type,value_constraints,description) VALUES ("enum",'{"enum":{"FAILURE":1,"WARNING":2,"SUCCESS":3}}',"the system operating voltage");
INSERT INTO config_table VALUES ("test.condition",2,1,8,1,0);
```
