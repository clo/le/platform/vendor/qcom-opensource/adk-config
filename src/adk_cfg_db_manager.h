/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ADK_CFG_DB_MANAGER_H_
#define SRC_ADK_CFG_DB_MANAGER_H_

#include <json-c/json.h>
#include <sqlite3.h>
#include <iostream>
#include <map>
#include <string>
#include <vector>
/**
  * @brief ConfigManager holds and maintains a connection to the sqlite
  * database file that is used for the configuration.
  *
  * The member functions provide abstraction on top of sqlite and the
  * schema used by the configuration system.
  *
  * InitialiseManager() should be called after constructing the ConfigManager
  * class.
  *
  */
namespace adk {
namespace config {

enum ConfigValueType { kUknown, kEnum, kBool, kDouble, kInt, kString };

class ConfigManager {
 public:
  /**
    * @brief Destructor, Will close the sqlite3 connection if it is open.
    */
  ~ConfigManager();

  /**
    * @brief Initialise the connection to a given database file. Will create a
    * connection to the database that will be closed when the destructor is
    * called.
    *
    * @returns True if initialisation was successful
    */
  bool InitialiseManager(const std::string &filename);

  /**
    * @brief Reads the config_meta_table and gets the version of the database.
    *
    * @returns True if database version could be read successfully
    * @param[out] return_string The string to copy the database version string
    * to
    */
  bool GetDatabaseVersion(std::string *return_string);

  /**
    * @brief Read the value of a given key-value pair.
    *
    * @param[out] return_value The value of the key-value pair
    * @param[in] read_key The key to read from
    *
    */
  bool ReadConfig(std::string *return_value, const std::string &read_key,
                  bool verbose = false);

  /**
    * @brief Get a list of all Key-value pairs in the database.
    *
    * @returns True if the config list was successfully retrieved
    * @param[in] show_constraints If true, returned strings will also contain
    * meta information for key-value pairs
    * @param[out] return_strings Vector containing a string for each row
    */
  bool ListConfig(bool show_constraints,
                  std::vector<std::string> *return_strings);

  /**
    * @brief Add a key-value pair to the database with a given meta_id. If no
    * constraints are to be applied to the key then the default meta_id of 1
    * will be used. The value will be checked against the constraints.
    *
    * @returns True if the new entry was successfully added
    * @returns False if the value is not within the constraints or the execution
    * failed
    *
    * @param[in] key The new key for the key-value pair
    * @param[in] value The new value for the key-value pair
    * @param[in] meta_id The meta_id for the new key-value pair
    */
  bool AddConfig(const std::string &key, const std::string &value,
                 bool ignore_constraints = true, int meta_id = 1);

  /**
    * @brief Write an input value to a given key. This will also check with the
    * constraints associated with that key to make sure the value fits within
    * the constraints. Will return false if this write fails.
    *
    * @returns True if write was successful
    * @param[out] return_value The value of the key-value pair
    * @param[in] key The key to write to
    * @param[in] value The value to write
    * @param[in] ignore_constraints If true, the write will ignore the
    * constraints in the meta table
    * @param[in] create If true, if the key does not exist, it will be created
    */
  bool WriteConfig(const std::string &key, const std::string &value,
                   bool ignore_constraints, bool create);

  /**
    * @brief Get a list of all key-values that start with the given key name.
    * This is equivalent to sql command "WHERE key LIKE "{KEY}%"
    *
    * @returns A vector containing a string for each returned result
    * @param[in] read_key The key string to match
    * @param[in] verbose True if returned results should contain verbose
    * information. i.e constraints, flags, description
    */
  bool ReadConfigLike(const std::string &read_key, bool verbose,
                      std::vector<std::string> *return_rows);

  /**
    * @brief Add to the meta table a new row with the value type,
    * constraint type and constraint string.
    *
    * @returns True If the meta entry was successfully added
    *
    * @param[in] val_type The value type: bool, int, double, string
    * @param[in] value_constraint JSON string containing the constraints
    * @param[in] description Meta Description
    * @param[out] row_id The row ID of the new meta entry
    */
  bool AddMeta(const std::string &val_type, const std::string &value_constraint,
               const std::string &description, int *row_id);

  /**
    * @brief Get a list of all the meta entries in the meta table.
    *
    * @returns True if the Meta list was successfully retrieved
    * @param[out] return_rows Vector containing a string for each row
    *
    */
  bool ListConfigMeta(std::vector<std::string> *return_rows);

  /**
    * @brief Get the JSON constraint string for at the given meta_id.
    *
    * @returns True If the constraint string was successfully retrieved
    *
    * @param[in] meta_id The meta_id to retrieve the constraint string for
    * @param[out] return_string The retrieved JSON constraint string
    */
  bool GetConstraintByID(int meta_id, std::string *return_string);

  /**
    * @brief Checks if the given value fits within the constraints
    * with the given meta_id.
    *
    * @returns True if the value is within the constraints
    * @param[in] meta_id The meta_id for the constraints entry in meta table
    *
    * @param[in] value The value to be verified
    */
  bool CheckConstraint(int meta_id, const std::string &value);
  /**
    * @brief Get the meta id for a given key.
    *
    * @returns True If the key was successfully retrieved
    *
    * @param[in] key The key to retrieve the meta_id from
    * @param[out] row_id The meta_id for the given key
    */
  bool GetConfigMetaID(const std::string &key, int *row_id);

  /**
    * @brief Get the JSON constraint string for the given key.
    *
    * @returns True If the constraint string was successfully retrieved
    *
    * @param[out] return_string The retrieved JSON constraint string
    * @param[in] key The key to retrieve the constraint string for
    */
  bool GetMetaByKey(std::string *return_string, const std::string &key);

  /**
    * @brief checks if given value type string e.g "str" is supported.
    *
    * @returns True if the type is supported
    * @param[in] value_type The value type to verify
    */
  bool IsValidType(const std::string &value_type) const;

  /**
    * @brief checks if the given constraint_type e.g "list" is supported.
    *
    * @returns True if the type is supported
    * @param[in] value_type The constraint type to verify
    */
  bool IsValidConstraintType(const std::string &constraint_type) const;

  /**
    * @brief Delete a key value pair entry that matches the input key.
    *
    * @returns True if key-value pair is successfully deleted
    * @param[in] key Key to delete
    * @param[in] delete_constraint If true will delete attempt to delete the
   * meta information aswell. If the foreign key constraint is violated this
   * will cause the function to return false
    */
  bool DeleteConfigKey(const std::string &key, bool delete_constraint);

  /**
    * @brief Reads the read_only flag of the key in the database.
    *
    * @returns True if key is read only
    */
  bool KeyIsReadOnly(const std::string &key);

  /**
    * @brief Reads the internal_only flag of the key in the database.
    *
    * @returns True if key is internal only
    */
  bool KeyIsInternalOnly(const std::string &key);

  /**
    * @brief Restores all keys in the database to the default stored value.
    *
    * @returns True if database was successfully restored
    *
    */
  bool RestoreDefault();

  /**
    * @brief Restores the given Key to its stored default value.
    *
    * @returns True if key was successfully restored
    *
    */
  bool RestoreDefault(const std::string &key);

  /**
    * @brief Add a value to the default value field in the configuration
    * database for a given key.
    *
    * @returns True if the default value was successfully added
    *
    */
  bool AddDefaultValue(const std::string &key, const std::string &value,
                       bool ignore_constraints = false);

  /**
    * @brief Checks if a given key exists in the database.
    *
    * @returns True if the key exists
    * @returns False if key does not exist or execution failed
    *
    */
  bool KeyExists(const std::string &key);

  /**
    * @brief Deletes a meta entry with the given ID. Will fail if the meta entry
   * is being used by a config value.
    *
    * @returns True if meta is successfully deleted
    * @param[in] meta_id The meta id for the entry to be deleted
    */
  bool DeleteMetaByID(int meta_id);

  /**
    * @brief Get if a meta entry exists at a given ID.
    *
    * @returns True if a meta entry exists with the given meta_id
    */
  bool MetaExistsByID(int meta_id);

  /**
    * @brief Modifies the value_constraint field for a given meta_id.
    *
    * @returns True If meta was successfully modified
    * @param[in] meta_id The meta id for the entry to be modified
    * @param[in] value_constraint replacement JSON constraint string
    */
  bool ModifyMetaByID(int meta_id, const std::string &value_constraint);

  /**
    * @brief Validate the input constraint string against the constraint JSON
    * schema.
    *
    * The JSON for the contraint must be in accordance to the following:
    *  "range": Array of objects, where each object contains two name/value
    * pairs "min" and "max". minimum and maximum range are inclusive. Multiple
    * ranges are allowed, ranges may overlap.
    * Example:
    * {"range":[{"min":1,"max":10}]}
    * {"range":[{"min":1,"max":10},{"min":30,"max":50}]}
    * "list": Array of values. Value must be an exact match for one of the items
    * in the list.
    * Example:
    * {"list":["cat","dog","cow"]}
    * {"list":[1,2,3]}
    * "enum": Object of name/value items. Constraint matching is done against
    * the name. The value is stored in the database
    * Example:
    * {"enum":{"FAILURE":1,"WARNING":2,"SUCCESS":3}
    * A constraint can contain multiple types specified as a list of objects.
    * Example:
    * {"range":[{"min":1,"max":10}], "list":[11,20,32]}
    *
    * @returns True If the constraint JSON schema was successfully validated
    * @param[in] constraint The JSON string to be validated.
    */
  bool ValidateConstraint(const std::string &constraint);

  /**
    * @brief Get the value type for the config key from the meta information.
    *
    * @returns True if getting the config value type executed successfully
    * @param[out] ConfigValueType Enum for the value type
    * @param[in] key The key to get the value type for
    */
  bool GetConfigValueType(const std::string &key,
                          adk::config::ConfigValueType *return_value);
  /**
    * @brief Get a map containing the enum item string - int mapping containined
    * in a JSON enum constraint string
    *
    * @returns True if getting the enum map was successfully
    * @param[in] json_constraint JSON enum constraint string
    * @param[out] enum_map Map containing the enum item string to int mapping
    */
  bool GetEnum(const std::string &json_constraint,
               std::map<std::string, int> *enum_map);

 private:
  /**
    *  @class Range
    *
    *  @brief A container class that holds two templated values min and max.
    *
    */
  template <class T>
  class Range {
   public:
    T min_;
    T max_;
    Range() {
      min_ = T();
      max_ = T();
    }
  };

  /**
    *  @variable kDefaultFilename_
    *  @brief Default file for opening database.
    */
  const std::string kDefaultFilename_ = "qsap.core.db";
  /**
    *  @variable kDefaultFilepath_
    *  @brief Default path for opening database.
    */
  const std::string kDefaultFilepath_ = "./";
  /**
    *  @variable kDefaultEnvironmentVariable_
    *  @brief Envinronment variable to to overide default file.
    */
  const char *kDefaultEnvironmentVariable_ = "QSAPDB_FILE";

  /**
    *  @variable kDefaultMetaID
    *  @brief Default Meta ID for unconstrained config keys.
    */
  const int kDefaultMetaID = 1;

  /**
    *  @variable database_location_
    *  @brief Contains the path to the database location.
    */
  std::string database_location_;

  /**
    *  @variable connection_
    *  @brief Connection object to the sqlite3 database.
    */
  sqlite3 *connection_ = nullptr;

 private:
  /**
    * @brief Opens the connection to the database stored in the database
    * location.
    *
    * @returns True If database was successfully opened
    */
  bool OpenDatabase();

  /**
    * @brief verifies that the opened database contains the correct tables as
   * specified by the schema
    *
    * @returns True If schema is correct
    */
  bool ValidateTables();

  /**
    * @brief Executes a given SQL query. Each result row will be on a new line.
    * This should only be called after initialising the connection to the
    * database.
    * each coloumn in the result is seperated by (|)
    *
    * @returns True if the sql query could be successfully executed
    * @param[in] sqlquery The SQL query to execute
    * @param[out] return_rows A vector of strings containing the a string for
    * each returned row from the sqlite query. If it is not required to check
    * the return rows the argument is defaulted to an empty list and doesnt need
    * to be provided
    *
    */
  bool ExecuteSqlQuery(const std::string &sqlquery,
                       std::vector<std::string> *return_rows = {});

  /**
    * @brief Checks if the integer value is within the json constraints object.
    *
    * @returns True if the value is within the JSON constraints
    * @param[in] constraint_jobj JSON constraint Object
    * @param[in] value Input value to check
    */
  bool CheckIntConstraint(json_object *constraint_jobj, int value);

  /**
    * @brief Checks if the double value is within the json constraints object
    *
    * @returns True if the value is within the JSON constraints
    * @param[in] constraint_jobj JSON constraint Object
    * @param[in] value Input value to check
    */
  bool CheckDoubleConstraint(json_object *constraint_jobj, double value);

  /**
    * @brief Checks if the string value is within the json constraints object
    *
    * @returns True if the value is within the JSON constraints
    * @param[in] constraint_jobj JSON constraint Object
    * @param[in] value Input value to check
    */
  bool CheckStringConstraint(json_object *constraint_jobj,
                             const std::string &value);

  /**
    * @brief Checks if the enum integer value is within the json constraints
   * object
    *
    * @returns True if the value is within the JSON constraints
    * @param[in] constraint_jobj JSON constraint Object
    * @param[in] value Input value to check
    */
  bool CheckEnumConstraint(json_object *constraint_jobj, int value);

  /**
    * @brief Get a Range object from a JSON object containing the correct meta
    * schema.
    *
    * @returns Returns an Range <T> object containing the min and the max values
    * passed from the json
    */
  template <class T>
  Range<T> JsonGetRange(json_object *jobj);
  // Returns vector of strings passed from the json object
  /**
    * @brief Get a vector containing each string in the JSON list object
    *
    * @returns A vector of strings for each item in the List
    * @param[in] jarray JSON array object containing listed items
    */
  std::vector<std::string> JsonGetStringList(json_object *jarray);

  /**
    * @brief Get a vector containing each int in the JSON list object
    *
    * @returns A vector of ints for each item in the List
    * @param[in] jarray JSON array object containing listed items
    */
  std::vector<int> JsonGetIntList(json_object *jarray);

  /**
    * @brief Get a vector containing each double in the JSON list object
    *
    * @returns A vector of doubles for each item in the List
    * @param[in] jarray JSON array object containing listed items
    */
  std::vector<double> JsonGetDoubleList(json_object *jarray);
};

/**
  * @brief Coerce the string to a bool
  * "True"/"true" = true
  *  "False"/"false" = false
  *  0 = false
  *  != 0 = true
  *
  * @returns True If conversion was successful
  * @param[in] input Input string
  * @param[out] value Converted input as bool
  */
bool CoerceToBool(const std::string &input, bool *value);
}  // namespace config
}  // namespace adk

#endif  // SRC_ADK_CFG_DB_MANAGER_H_
