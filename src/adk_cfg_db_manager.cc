/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "adk_cfg_db_manager.h"
#include <json-c/json.h>
#include <algorithm>
#include <fstream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
namespace adk {
namespace config {
ConfigManager::~ConfigManager() {
  if (connection_ != nullptr) {
    // Check if the database connection has been successfully closed.
    if (sqlite3_close(connection_) != SQLITE_OK) {
      std::cerr << "ERROR: SQLITE3 database failed to close" << std::endl;
    }
  }
}

bool ConfigManager::InitialiseManager(const std::string &db_filepath) {
  // Check if the file exists.
  std::ifstream check_file(db_filepath.c_str());
  if (check_file.good()) {
    database_location_ = db_filepath;
  } else {
    std::cerr << "ERROR: Could not find file: " << db_filepath << std::endl;
    return false;
  }

  if (!OpenDatabase()) {
    std::cerr << "ERROR: Could not open config database" << std::endl;
    return false;
  }

  return true;
}

bool ConfigManager::GetDatabaseVersion(std::string *return_string) {
  std::string version_query =
      "SELECT value FROM database_meta_table "
      "WHERE key = \'database_version\'";

  std::vector<std::string> query_results;
  // Get version number from database_meta_table
  if (!ExecuteSqlQuery(version_query, &query_results)) {
    return false;
  }
  if (query_results.size() == 0) {
    return false;
  }
  *return_string = query_results.front();
  return true;
}

bool ConfigManager::ReadConfig(std::string *return_value,
                               const std::string &read_key, bool verbose) {
  // Return false if the key does not exist
  if (!KeyExists(read_key)) {
    return false;
  }
  std::string sql_query;
  // If verbose read all of the column values
  if (verbose) {
    sql_query =
        "SELECT key,value,value_default,read_only,"
        "internal_only,value_type,value_constraints,description"
        " from config_table join meta_table on "
        "config_table.meta_id = meta_table.id"
        " WHERE key=\'" +
        read_key + "\'";
  } else {
    // If not verbose only read the value
    sql_query = "select value from config_table WHERE key=\'" + read_key + "\'";
  }

  std::vector<std::string> query_results;
  if (!ExecuteSqlQuery(sql_query, &query_results)) {
    return false;
  }
  if (query_results.size() == 0) {
    return false;
  }
  // Set the return_value to the first sql row response
  *return_value = query_results.front();
  return true;
}

bool ConfigManager::ListConfig(bool show_constraints,
                               std::vector<std::string> *return_strings) {
  if (show_constraints) {
    // If showing constraints join the config_table to the meta_table and select
    // columns from both tables
    std::string sql_query =
        "select"
        " key,value,value_default,read_only,internal_only,"
        " value_type,value_constraints,description"
        " from config_table join meta_table on"
        " config_table.meta_id = meta_table.id";

    if (!ExecuteSqlQuery(sql_query, return_strings)) {
      return false;
    }

  } else {
    std::string sql_query = "select key,value from config_table";
    if (!ExecuteSqlQuery(sql_query, return_strings)) {
      return false;
    }
  }

  return true;
}

bool ConfigManager::AddConfig(const std::string &key, const std::string &value,
                              bool ignore_constraints, int meta_id) {
  // Check if the provided meta_id exists
  if (!MetaExistsByID(meta_id)) {
    std::cerr << "ERROR: no meta exists with given ID" << std::endl;
    return false;
  }
  if (!ignore_constraints) {
    // Check the value fits within the constraints at the given meta_id
    if (!CheckConstraint(meta_id, value)) {
      std::cerr << "ERROR: value not within meta_table at given ID"
                << std::endl;
      return false;
    }
  }
  // Inserted value is not read-only and not internal-only.
  std::string add_query =
      "INSERT INTO config_table"
      "(read_only,internal_only,key,value,value_default,meta_id)"
      " VALUES(0,0,\'" +
      key + "\', \'" + value + "\', \'" + value + "\', " +
      std::to_string(meta_id) + ")";
  if (!ExecuteSqlQuery(add_query)) {
    return false;
  }

  return true;
}

bool ConfigManager::WriteConfig(const std::string &write_key,
                                const std::string &value,
                                bool ignore_constraints, bool create) {
  //  Check if the input value is within the constraints and modify the
  //  Key if that is the case.
  if (KeyExists(write_key)) {
    if (!ignore_constraints) {
      int meta_id;
      if (!GetConfigMetaID(write_key, &meta_id)) {
        return false;
      }
      if (!CheckConstraint(meta_id, value)) {
        return false;
      }
    }

    std::string sql_query = "UPDATE config_table SET value=\'" + value +
                            "\' WHERE key =\'" + write_key + "\'";
    if (!ExecuteSqlQuery(sql_query)) {
      return false;
    }
    return true;

  } else {
    // if the key doesn't exists and create is true then add the key
    if (create) {
      return AddConfig(write_key, value);
    } else {
      return false;
    }
  }
  return true;
}

bool ConfigManager::ReadConfigLike(const std::string &read_key, bool verbose,
                                   std::vector<std::string> *return_rows) {
  std::string sql_query;
  if (verbose) {
    // Select all result coloums if verbose
    sql_query =
        "SELECT key,value,value_default,read_only,internal_only,"
        "value_type,value_constraints,description"
        " from config_table join meta_table on "
        "config_table.meta_id = meta_table.id WHERE key LIKE \'" +
        read_key + "%\'";
  } else {
    sql_query = "SELECT key,value FROM config_table WHERE key LIKE  \'" +
                read_key + "%\'";
  }
  if (!ExecuteSqlQuery(sql_query, return_rows)) {
    return false;
  }
  return true;
}

bool ConfigManager::AddMeta(const std::string &value_type,
                            const std::string &value_constraint,
                            const std::string &description, int *row_id) {
  //  Check that input types are valid
  if (!IsValidType(value_type)) {
    std::cerr << "ERROR: not a valid value type" << std::endl;
    return false;
  }
  if (!ValidateConstraint(value_constraint)) {
    std::cerr << "ERROR: Constraint not valid" << std::endl;
    return false;
  }

  std::string add_query =
      "INSERT INTO meta_table("
      "value_type,value_constraints,description) VALUES(\'" +
      value_type + "\',\'" + value_constraint + "\',\'" + description + "\')";
  ExecuteSqlQuery(add_query);

  //  Get row ID of inserted constraint
  std::string constraint_query =
      "SELECT id FROM meta_table ORDER BY id DESC LIMIT 1";
  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(constraint_query, &query_results)) {
    return false;
  }
  if (query_results.size() == 0) {
    return false;
  }
  int meta_id;
  // Make sure that the meta_id is an int
  try {
    meta_id = std::stoi(query_results.front());
  } catch (const std::invalid_argument &ia) {
    return false;
  } catch (const std::out_of_range &oa) {
    return false;
  }
  *row_id = meta_id;
  return true;
}

bool ConfigManager::ListConfigMeta(std::vector<std::string> *return_rows) {
  std::string sql_query =
      "SELECT id,value_type,value_constraints,description FROM meta_table";

  if (!ExecuteSqlQuery(sql_query, return_rows)) {
    return false;
  }
  return true;
}

bool ConfigManager::GetConstraintByID(int meta_id, std::string *return_string) {
  std::string constraint_query =
      "SELECT value_constraints FROM meta_table WHERE id =" +
      std::to_string(meta_id);

  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(constraint_query, &query_results)) {
    return false;
  }
  if (query_results.size() == 0) {
    return false;
  }
  // Return the first result from the query
  *return_string = query_results.front();
  return true;
}

bool ConfigManager::CheckConstraint(int meta_id, const std::string &value) {
  std::string get_constraints_query =
      "SELECT value_constraints FROM meta_table WHERE id =" +
      std::to_string(meta_id);
  std::string get_value_type_query =
      "SELECT value_type FROM meta_table WHERE id = " + std::to_string(meta_id);

  std::vector<std::string> constraint_query_results;
  // Get the JSON constraint string to check against
  if (!ExecuteSqlQuery(get_constraints_query, &constraint_query_results)) {
    return false;
  }
  // Make sure that the result is not empty
  if (constraint_query_results.size() == 0) {
    return false;
  }
  // Convert constraint string to a JSON-C object
  json_object *constraint_jobj =
      json_tokener_parse(constraint_query_results.front().c_str());

  // Get the value type for the constraint
  std::vector<std::string> value_query_results;

  if (!ExecuteSqlQuery(get_value_type_query, &value_query_results)) {
    return false;
  }
  if (value_query_results.size() == 0) {
    return false;
  }

  std::string value_type = value_query_results.front();

  // Make sure that the JSON could be successfully converted. i.e valid JSON
  if (constraint_jobj == nullptr) {
    std::cerr << "ERROR: constraints is not parseable JSON" << std::endl;
    return false;
  }
  // If constraint type is int, try to convert the value to an int then check
  // against the constraint object
  if (value_type == "int") {
    int int_value;
    try {
      int_value = std::stoi(value);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
    return CheckIntConstraint(constraint_jobj, int_value);
  }
  //  float value_types included for legacy database files
  // If constraint type is double or float try to convert the value to a double
  // then check against the constraint object
  if (value_type == "double" || value_type == "float") {
    double double_value;
    try {
      double_value = std::stod(value);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
    return CheckDoubleConstraint(constraint_jobj, double_value);
  }
  // If constraint type is string, no conversion is neccessary
  if (value_type == "str") {
    return CheckStringConstraint(constraint_jobj, value);
  }
  // If constraint type is enum, try to convert the value to an int then check
  // against the constraint object
  if (value_type == "enum") {
    int int_value;
    try {
      int_value = std::stoi(value);
    } catch (const std::invalid_argument &ia) {
      return false;
    } catch (const std::out_of_range &oa) {
      return false;
    }
    return CheckEnumConstraint(constraint_jobj, int_value);
  }

  // If constraint is none of these types, return false;
  return false;
}

bool ConfigManager::GetConfigMetaID(const std::string &key, int *row_id) {
  std::string sql_query =
      "SELECT meta_id FROM config_table WHERE key =\"" + key + "\"";

  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(sql_query, &query_results)) {
    return false;
  }
  if (query_results.size() == 0) {
    return false;
  }

  int id;
  // Ensure that the meta_id is an int
  try {
    id = std::stoi(query_results.front());
  } catch (const std::invalid_argument &ia) {
    return false;
  } catch (const std::out_of_range &oa) {
    return false;
  }
  *row_id = id;
  return true;
}

bool ConfigManager::GetMetaByKey(std::string *return_string,
                                 const std::string &key) {
  int meta_id;
  if (!GetConfigMetaID(key, &meta_id)) {
    std::cerr << "ERROR no matching key" << std::endl;
    return false;
  }
  // Get the meta coloumns based on the meta_id
  std::string constraint_query =
      "SELECT id,value_type,value_constraints,description "
      "FROM meta_table WHERE id=" +
      std::to_string(meta_id);
  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(constraint_query, &query_results)) {
    return false;
  }
  // If no results are returned, return false
  if (query_results.size() == 0) {
    return false;
  }
  // Return first result
  *return_string = query_results.front();
  return true;
}

bool ConfigManager::IsValidType(const std::string &value_type) const {
  std::set<std::string> value_type_set = {"double", "float", "str",
                                          "int",    "enum",  "bool"};
  // Check if value type is within set
  if (value_type_set.find(value_type) == value_type_set.end()) {
    return false;
  }
  return true;
}

bool ConfigManager::IsValidConstraintType(
    const std::string &constraint_type) const {
  std::set<std::string> constraint_type_set = {"list", "range", "enum"};
  // Check if constraint type is within set
  if (constraint_type_set.find(constraint_type) == constraint_type_set.end()) {
    return false;
  }
  return true;
}

bool ConfigManager::DeleteConfigKey(const std::string &key,
                                    bool delete_constraint) {
  int meta_id;
  if (!GetConfigMetaID(key, &meta_id)) {
    return false;
  }
  // First delete the key value pair and then try and delete the constraint
  // associated with it. If the constraint is in use by another key value pair
  // then the constraint will not be deleted.
  std::string sql_query =
      "DELETE FROM config_table WHERE key = \'" + key + "\'";
  ExecuteSqlQuery(sql_query);

  if (delete_constraint) {
    sql_query = "DELETE FROM meta_table WHERE id = " + std::to_string(meta_id);
    ExecuteSqlQuery(sql_query);
  }
  return true;
}

bool ConfigManager::KeyIsReadOnly(const std::string &key) {
  if (!KeyExists(key)) {
    return false;
  }
  std::string read_only_query =
      "SELECT read_only FROM config_table"
      " WHERE key=\'" +
      key + "\'";
  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(read_only_query, &query_results)) {
    return false;
  }
  // If no results are returned, return false
  if (query_results.size() == 0) {
    return false;
  }
  bool is_read_only;
  // Account for the flag being "1" or "true" or "TRUE" etc...
  CoerceToBool(query_results.front(), &is_read_only);
  return is_read_only;
}

bool ConfigManager::KeyIsInternalOnly(const std::string &key) {
  if (!KeyExists(key)) {
    return false;
  }
  std::string internal_only_query =
      "SELECT internal_only FROM config_table"
      " WHERE key=\'" +
      key + "\'";

  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(internal_only_query, &query_results)) {
    return false;
  }
  // If no results are returned, return false
  if (query_results.size() == 0) {
    return false;
  }
  bool is_internal_only;
  // Account for the flag being "1" or "true" or "TRUE" etc...
  CoerceToBool(query_results.front(), &is_internal_only);
  return is_internal_only;
}

bool ConfigManager::RestoreDefault() {
  std::string reset_query = "UPDATE config_table SET value = value_default";
  if (!ExecuteSqlQuery(reset_query)) {
    return false;
  }
  return true;
}

bool ConfigManager::RestoreDefault(const std::string &read_key) {
  if (KeyExists(read_key)) {
    std::string reset_query =
        "UPDATE config_table SET value = value_default "
        "WHERE key = \'" +
        read_key + "\'";
    if (!ExecuteSqlQuery(reset_query)) {
      return false;
    }
    return true;
  }
  // Return false if the key does not exist
  return false;
}

bool ConfigManager::AddDefaultValue(const std::string &key,
                                    const std::string &value,
                                    bool ignore_constraints) {
  if (!KeyExists(key)) {
    return false;
  }
  // Get the config items meta_id
  int meta_id;
  if (!GetConfigMetaID(key, &meta_id)) {
    return false;
  }
  // Don't check the constraints if ignore_constraints is true
  if (!ignore_constraints) {
    if (!CheckConstraint(meta_id, value)) {
      return false;
    }
  }
  std::string add_query =
      "UPDATE config_table SET value_default ="
      " \'" +
      value + "\' WHERE key = \'" + key + "\'";
  if (!ExecuteSqlQuery(add_query)) {
    return false;
  }
  return true;
}

bool ConfigManager::KeyExists(const std::string &read_key) {
  std::string sql_query =
      "SELECT value FROM config_table WHERE key=\'" + read_key + "\'";
  std::vector<std::string> query_results;
  if (!ExecuteSqlQuery(sql_query, &query_results)) {
    return false;
  }
  // If there are no results then meta does not exist
  if (query_results.size() == 0) {
    return false;
  }
  return true;
}

bool ConfigManager::DeleteMetaByID(int meta_id) {
  if (MetaExistsByID(meta_id)) {
    std::string sql_query =
        "DELETE FROM meta_table WHERE id = " + std::to_string(meta_id);
    ExecuteSqlQuery(sql_query);
    return true;
  }
  // If the meta does not exist then return false;
  return false;
}

bool ConfigManager::MetaExistsByID(int meta_id) {
  std::string sql_query =
      "SELECT id FROM meta_table WHERE id=" + std::to_string(meta_id);
  std::vector<std::string> query_results;
  if (!ExecuteSqlQuery(sql_query, &query_results)) {
    return false;
  }
  // If there are no results then meta does not exist
  if (query_results.size() == 0) {
    return false;
  }
  return true;
}

bool ConfigManager::ModifyMetaByID(int meta_id,
                                   const std::string &value_constraint) {
  // Check if meta exists
  if (!MetaExistsByID(meta_id)) {
    return false;
  }
  std::string modify_query = "UPDATE meta_table SET value_constraints=\'" +
                             value_constraint + "\' WHERE id=" +
                             std::to_string(meta_id);
  // Return false if the sql could not be executed
  if (!ExecuteSqlQuery(modify_query)) {
    return false;
  }
  return true;
}

bool ConfigManager::ValidateConstraint(const std::string &constraint) {
  // Check if JSON string is valid JSON
  json_object *constraint_jobj = json_tokener_parse(constraint.c_str());
  if (constraint_jobj == nullptr) {
    std::cerr << "ERROR: Constraint string is not valid JSON" << std::endl;
    return false;
  }

  std::set<std::string> constraint_type_set = {"range", "list", "enum"};
  bool is_empty = true;
  json_object_object_foreach(constraint_jobj, key, val) {
    is_empty = false;
    // Check if constraint type is valid
    if (constraint_type_set.find(key) == constraint_type_set.end()) {
      std::cerr
          << "ERROR: \"" << key
          << "\" is not a valid constraint type. Valid types are: \"range\", "
             "\"list\", \"enum\""
          << std::endl;
      return false;
    }
    // If constraint type is range then validate against schema
    if (std::string(key) == "range") {
      enum json_type type;
      type = json_object_get_type(val);
      // Check if range contains a list
      if (type != json_type_array) {
        std::cerr << "ERROR: Range constraint is not an array" << std::endl;
        return false;
      }
      json_object *jarray = val;
      int array_len = json_object_array_length(jarray);
      // Check if array is empty
      if (array_len == 0) {
        std::cerr << "ERROR: Range list is empty" << std::endl;
        return false;
      }
      bool contains_max, contains_min;
      // For each object in the array check that it contains a min and a max
      // value.
      for (int i = 0; i < array_len; i++) {
        contains_max = false;
        contains_min = false;
        json_object *array_obj = json_object_array_get_idx(jarray, i);
        json_object_object_foreach(array_obj, key2, val2) {
          if (std::string(key2) == "min") {
            contains_min = true;
          }
          if (std::string(key2) == "max") {
            contains_max = true;
          }
        }
        if (!(contains_max && contains_min)) {
          std::cerr << "ERROR: range does not contain min and max" << std::endl;
          return false;
        }
      }
      // If constraint type is list then validate against schema
    } else if (std::string(key) == "list") {
      // Check if list contains a list
      if (json_object_get_type(val) != json_type_array) {
        std::cerr << "ERROR: List constraint is not an array" << std::endl;
        return false;
      }
      json_object *jarray = val;
      int array_len = json_object_array_length(jarray);
      // Check if array is empty
      if (array_len == 0) {
        std::cerr << "ERROR: List is empty" << std::endl;
        return false;
      }
      for (int i = 0; i < array_len; i++) {
        // Check each object in list and verify that it contains a value
        json_object *array_obj = json_object_array_get_idx(jarray, i);
        enum json_type type;
        type = json_object_get_type(array_obj);

        if (type != json_type_double && type != json_type_int &&
            type != json_type_string) {
          return false;
        }
      }
      // If constraint type is enum then validate against schema
    } else if (std::string(key) == "enum") {
      // check that enum containst a json object of key value pairs
      enum json_type type = json_object_get_type(val);
      if (type != json_type_object) {
        std::cerr << "ERROR: enum object is not formatted an object"
                  << std::endl;
        return false;
      }
      // Check that object contains key value pairs with the value being an int
      json_object_object_foreach(val, key2, val2) {
        // Verify that the enum value is an int.
        if (json_object_get_type(val2) != json_type_int) {
          std::cerr
              << "ERROR: Enum object does not contain key, int-value pairs"
              << std::endl;
          return false;
        }
      }
    }
  }
  if (is_empty) {
    std::cerr << "ERROR: JSON is empty" << std::endl;
    return false;
  }
  return true;
}

bool ConfigManager::GetConfigValueType(const std::string &key,
                                       ConfigValueType *return_value) {
  int meta_id;
  if (!GetConfigMetaID(key, &meta_id)) {
    return false;
  }

  std::string sql_query =
      "SELECT value_type FROM meta_table WHERE id=" + std::to_string(meta_id);

  std::vector<std::string> query_results;

  if (!ExecuteSqlQuery(sql_query, &query_results)) {
    return false;
  }

  std::string type_string = query_results.front();

  if (type_string == "enum") {
    *return_value = kEnum;
  } else if (type_string == "int") {
    *return_value = kInt;
  } else if (type_string == "bool") {
    *return_value = kBool;
  } else if (type_string == "double") {
    *return_value = kDouble;
  } else if (type_string == "float") {
    *return_value = kDouble;
  } else if (type_string == "str") {
    *return_value = kString;
  } else {
    *return_value = kUknown;
  }
  return true;
}

bool ConfigManager::GetEnum(const std::string &json_constraint,
                            std::map<std::string, int> *enum_map) {
  json_object *constraint_jobj = json_tokener_parse(json_constraint.c_str());
  if (constraint_jobj == NULL) {
    return false;
  }
  json_object_object_foreach(constraint_jobj, key, val) {
    if (std::string(key) == "enum") {
      json_object_object_foreach(val, key2, val2) {
        enum_map->insert({key2, json_object_get_int(val2)});
      }
    } else {
      return false;
    }
  }
  return true;
}
bool ConfigManager::OpenDatabase(void) {
  // Open sqlite3 database
  int result = sqlite3_open(database_location_.c_str(), &connection_);
  if (result != SQLITE_OK) {
    std::cerr << "ERROR (sqlite): " << sqlite3_errmsg(connection_) << std::endl;
    sqlite3_close(connection_);
    return false;
  }
  // Check if the database contains the correct tables acording to the schema
  if (!ValidateTables()) {
    std::cerr << "ERROR: Unable to validate tables" << std::endl;
    return false;
  }

  // Ensure that foreign_keys are enabled to stop constraints being deleted that
  // are in use.
  if (!ExecuteSqlQuery("pragma foreign_keys=ON")) {
    return false;
  }

  return true;
}

bool ConfigManager::ValidateTables(void) {
  // Check the tables exists in the sqlite_master table
  std::string validate_config_query =
      "SELECT name FROM sqlite_master "
      "WHERE type = \'table\' AND name=\'config_table\'";

  std::string validate_constraints_query =
      "SELECT name FROM sqlite_master "
      "WHERE type = \'table\' AND name=\'meta_table\'";

  std::string validate_db_meta_table_query =
      "SELECT name FROM sqlite_master "
      "WHERE type = \'table\' AND name=\'database_meta_table\'";

  // Check if config table exists
  std::vector<std::string> config_query_results;
  if (!ExecuteSqlQuery(validate_config_query, &config_query_results)) {
    return false;
  }
  if (config_query_results.size() == 0) {
    return false;
  }

  // Check if constraints table exists
  std::vector<std::string> constraints_query_results;
  if (!ExecuteSqlQuery(validate_constraints_query,
                       &constraints_query_results)) {
    return false;
  }
  if (constraints_query_results.size() == 0) {
    return false;
  }

  // Check if database_meta_table table exists
  std::vector<std::string> db_meta_query_results;
  if (!ExecuteSqlQuery(validate_db_meta_table_query, &db_meta_query_results)) {
    return false;
  }
  if (db_meta_query_results.size() == 0) {
    return false;
  }
  return true;
}
bool ConfigManager::ExecuteSqlQuery(const std::string &sqlquery,
                                    std::vector<std::string> *return_rows) {
  if (connection_ == nullptr) {
    std::cerr << "ERROR:connection closed" << std::endl;
    return false;
  }
  sqlite3_stmt *query;

  // Prepare the sqlite3 query and put results in query object

  if (sqlite3_prepare_v2(connection_, sqlquery.c_str(), -1, &query, nullptr) !=
      SQLITE_OK) {
    std::cerr << "ERROR (sqlite): " << sqlite3_errmsg(connection_);
    return false;
  }
  int step_result;
  //  Get every row from the query. result will stop being SQLITE_ROW when
  //  there are no more rows in the result
  while (SQLITE_ROW == (step_result = sqlite3_step(query))) {
    std::string row_string;
    int column_count = sqlite3_column_count(query);
    for (int i = 0; i < column_count; i++) {
      if (sqlite3_column_text(query, i) != nullptr) {
        if (i != 0) {
          row_string += "|";
        }
        row_string += std::string(
            reinterpret_cast<const char *>(sqlite3_column_text(query, i)));
      }
    }
    return_rows->push_back(row_string);
  }

  bool return_value = true;
  //  give error if the end of the SQLITE query is not SQLITE_DONE
  if (step_result != SQLITE_DONE) {
    std::cerr << "ERROR: " << step_result << std::endl;
    return_value = false;
  }
  // Close the query object. This must be done otherwise the sqlite connection
  // cannot close
  if (sqlite3_finalize(query) != SQLITE_OK) {
    std::cerr << "ERROR: SQLITE3 Statement not destructed" << std::endl;
    return_value = false;
  }
  return return_value;
}

bool ConfigManager::CheckIntConstraint(json_object *constraint_jobj,
                                       int value) {
  // Verify against each constraint object
  json_object_object_foreach(constraint_jobj, key, val) {
    // Check if value is within a list object
    if (std::string(key) == "list") {
      std::vector<int> int_list = JsonGetIntList(val);
      if (int_list.size() != 0) {
        std::vector<int>::iterator int_iterator;
        // If value is found within the list then return true
        int_iterator = find(int_list.begin(), int_list.end(), value);
        if (int_iterator != int_list.end()) {
          return true;
        }
      }
    }
    // Check if value is within a range object
    if (std::string(key) == "range") {
      enum json_type j_type = json_object_get_type(val);
      // A range object must contain a list of min,max objects
      if (j_type == json_type_array) {
        json_object *j_array = val;
        int array_length = json_object_array_length(j_array);
        // For each min,max object in the list check if the value is
        // within the range
        for (int i = 0; i < array_length; i++) {
          json_object *array_obj = json_object_array_get_idx(j_array, i);
          Range<int> int_range = JsonGetRange<int>(array_obj);
          if (value >= int_range.min_ && value <= int_range.max_) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

bool ConfigManager::CheckDoubleConstraint(json_object *constraint_jobj,
                                          double value) {
  // Verify against each constraint object
  json_object_object_foreach(constraint_jobj, key, val) {
    // Check if value is within a list object
    if (std::string(key) == "list") {
      std::vector<double> double_list = JsonGetDoubleList(val);
      if (double_list.size() != 0) {
        std::vector<double>::iterator double_iterator;
        // If value is found within the list then return true
        double_iterator = find(double_list.begin(), double_list.end(), value);
        if (double_iterator != double_list.end()) {
          return true;
        }
      }
    }
    // Check if value is within a range object
    if (std::string(key) == "range") {
      enum json_type j_type = json_object_get_type(val);
      // A range object must contain a list of min,max objects
      if (j_type == json_type_array) {
        json_object *jarray = val;
        int array_len = json_object_array_length(jarray);
        // For each min,max object in the list check if the value is
        // within the range
        for (int i = 0; i < array_len; i++) {
          json_object *array_obj = json_object_array_get_idx(jarray, i);
          Range<double> double_range = JsonGetRange<double>(array_obj);
          if (value >= double_range.min_ && value <= double_range.max_) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

bool ConfigManager::CheckStringConstraint(json_object *constraint_jobj,
                                          const std::string &value) {
  // Check if value is within a range object
  json_object_object_foreach(constraint_jobj, key, val) {
    // Check if value is within a list object
    if (std::string(key) == "list") {
      std::vector<std::string> string_list = JsonGetStringList(val);
      if (string_list.size() != 0) {
        std::vector<std::string>::iterator string_iterator;
        // If value is found within the list then return true
        string_iterator = find(string_list.begin(), string_list.end(), value);
        if (string_iterator != string_list.end()) {
          return true;
        }
      }
    }
  }
  return false;
}

bool ConfigManager::CheckEnumConstraint(json_object *constraint_jobj,
                                        int value) {
  json_object_object_foreach(constraint_jobj, key, val) {
    if (std::string(key) == "enum") {
      // Check if value exists in any of the key:value pairs.
      json_object_object_foreach(val, key2, val2) {
        if (value == json_object_get_int(val2)) {
          return true;
        }
      }
    }
  }
  return false;
}

template <class T>
ConfigManager::Range<T> ConfigManager::JsonGetRange(json_object *jobj) {
  Range<T> return_range = Range<T>();
  // Get the min and max values and store them in the range class
  json_object_object_foreach(jobj, key, val) {
    if (std::string(key) == "min") {
      enum json_type type;
      // Convert the min value appropriately
      type = json_object_get_type(val);

      if (type == json_type_int) {
        return_range.min_ = json_object_get_int(val);
      }
      if (type == json_type_double) {
        return_range.min_ = json_object_get_double(val);
      }
    }
    if (std::string(key) == "max") {
      enum json_type type;
      // Convert the min value appropriately
      type = json_object_get_type(val);

      if (type == json_type_int) {
        return_range.max_ = json_object_get_int(val);
      }
      if (type == json_type_double) {
        return_range.max_ = json_object_get_double(val);
      }
    }
  }
  return return_range;
}

std::vector<std::string> ConfigManager::JsonGetStringList(json_object *jarray) {
  std::vector<std::string> return_list;
  enum json_type type;
  type = json_object_get_type(jarray);

  if (type == json_type_array) {
    int array_len = json_object_array_length(jarray);
    for (int i = 0; i < array_len; i++) {
      json_object *array_obj = json_object_array_get_idx(jarray, i);
      return_list.push_back(json_object_get_string(array_obj));
    }
  }

  return return_list;
}

std::vector<int> ConfigManager::JsonGetIntList(json_object *jarray) {
  std::vector<int> return_list;
  enum json_type type;
  type = json_object_get_type(jarray);

  if (type == json_type_array) {
    int array_len = json_object_array_length(jarray);
    for (int i = 0; i < array_len; i++) {
      json_object *array_obj = json_object_array_get_idx(jarray, i);
      return_list.push_back(json_object_get_int(array_obj));
    }
  }

  return return_list;
}

std::vector<double> ConfigManager::JsonGetDoubleList(json_object *jarray) {
  std::vector<double> return_list;
  enum json_type type;
  type = json_object_get_type(jarray);

  if (type == json_type_array) {
    int array_len = json_object_array_length(jarray);
    for (int i = 0; i < array_len; i++) {
      json_object *array_obj = json_object_array_get_idx(jarray, i);
      return_list.push_back(json_object_get_double(array_obj));
    }
  }

  return return_list;
}

bool CoerceToBool(const std::string &input, bool *value) {
  // first check if the string can be converted to an integer
  // If it can't be converted to an integer, then check if it contains
  // any strings which look like boolean types. e.g. "true", "False"
  try {
    *value = std::stoi(input);
    return true;
    // the string contained a recognisable integer
  } catch (const std::invalid_argument &ia) {
    return false;
  } catch (const std::out_of_range &oa) {
    return false;
  }
  // use a map as a lookup table
  std::map<std::string, bool> TrueFalseMap = {
      {"true", true},   {"True", true},   {"TRUE", true},
      {"false", false}, {"False", false}, {"FALSE", false}};
  if (TrueFalseMap.count(input)) {
    *value = TrueFalseMap[input];
  } else {
    return false;
  }
  return true;
}
}  // namespace config
}  // namespace adk
