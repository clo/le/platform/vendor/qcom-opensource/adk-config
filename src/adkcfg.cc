/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "adkcfg.h"

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "adk_cfg_db_manager.h"
#define kDefaultEnvironmentVariable "ADK_CONFIG_FILE"
using adk::config::ConfigManager;
int main(int argc, char* argv[]) {
  //  Long options structure
  const struct option kLongOptions[] = {
      {"help", no_argument, 0, 'h'},
      {"like", no_argument, 0, 'l'},
      {"file", required_argument, 0, 'f'},
      {"all", no_argument, 0, 'a'},
      {"key", required_argument, 0, 'k'},
      {"list", no_argument, 0, 'l'},
      {"version", no_argument, 0, 'v'},
      {"ignore", no_argument, 0, 'i'},
      {"default", required_argument, 0, 'd'},
      {"meta", required_argument, 0, 'm'},
      {"delete", required_argument, 0, 'd'},
      {"modify", required_argument, 0, 'm'},
      {"verbose-help", no_argument, 0, 'w'},
      {"no-heading", no_argument, 0, 'n'},
      {"no-create", no_argument, 0, 'c'},
      {0, 0, 0, 0},
  };

  std::string database_location;

  // flags used to control
  bool aflag = false;
  bool fflag = false;
  bool lflag = false;
  bool kflag = false;
  bool vflag = false;
  bool iflag = false;
  bool dflag = false;
  bool mflag = false;
  bool hflag = false;
  bool wflag = false;
  bool nflag = false;
  bool cflag = false;
  std::string koptions;
  std::string doptions;
  int moptions;
  //  If no command is given give prompt to user
  if (argc < 2) {
    PrintHelp();
    exit(EXIT_SUCCESS);
  }
  // Get the command line options provided
  int option_char = 0;
  int index = 0;
  while ((option_char = getopt_long(argc, argv, "nd:ck:ahlf:vim:w",
                                    kLongOptions, &index)) != -1) {
    // Check and set the option flags and option variables
    switch (option_char) {
      case 'f': {
        std::string foptions(optarg);
        database_location = foptions;
        fflag = true;
        break;
      }
      case 'c':
        cflag = true;
        break;

      case 'l':
        lflag = true;
        break;

      case 'n':
        nflag = true;
        break;

      case 'a':
        aflag = true;
        break;

      case 'h':
        hflag = true;
        break;

      case 'w':
        wflag = true;
        break;

      case 'k':
        kflag = true;
        koptions = std::string(optarg);
        break;

      case 'm':
        mflag = true;
        try {
          moptions = std::stoi(optarg);
        } catch (const std::invalid_argument& ia) {
          std::cerr << "ERROR: -m requires an integer" << std::endl;
          exit(EXIT_FAILURE);
        } catch (const std::out_of_range& oa) {
          std::cerr << "ERROR: Integer out of range" << std::endl;
          exit(EXIT_FAILURE);
        }
        break;

      case 'd':
        dflag = true;
        doptions = std::string(optarg);
        break;
      case 'v':
        vflag = true;
        break;
      case 'i':
        iflag = true;
        break;
      case ':':
        // Missing option argument
        exit(EXIT_FAILURE);
        break;
      case '?':
        // Missing argument or argument doesn't exist.
        exit(EXIT_FAILURE);
        break;
      default:
        break;
    }
  }
  if (hflag) {
    PrintHelp();
    exit(EXIT_SUCCESS);
  }
  if (wflag) {
    PrintVerboseHelp();
    exit(EXIT_SUCCESS);
  }

  if (argv[optind] == NULL) {
    if (vflag) {
      PrintVersionHeader();
      exit(EXIT_SUCCESS);
    }
  }

  //  Initialise config_manager and sqlite connection
  //  And exit if config_manager cannot be initialised.
  ConfigManager config_manager;

  if (!fflag) {
    char* adkdb_file = std::getenv(kDefaultEnvironmentVariable);

    if (adkdb_file == NULL) {
      std::cerr << "ERROR: No default configuration environment variable set. "
                   "Please Set "
                << kDefaultEnvironmentVariable
                << ". Alternatively use -f <FILENAME>." << std::endl;
      exit(EXIT_FAILURE);
    }

    database_location = adkdb_file;
  }
  if (!config_manager.InitialiseManager(database_location)) {
    std::cerr << "ERROR: Unable to initialise database manager" << std::endl;
    exit(EXIT_FAILURE);
  }

  //  Get the command and check versus implemented commands. if it doesnt exists
  //  notify the user
  std::string command_string;
  if (argv[optind] != NULL) {
    // Command is the first argument
    command_string = std::string(argv[optind]);

    if (command_string == "read") {
      // Read requires a key to read
      if (argv[optind + 1] == NULL) {
        std::cerr << "ERROR: no key given to read" << std::endl;
      } else {
        std::string key_arg = argv[(optind + 1)];
        // If --like and --all
        if (lflag && aflag) {
          if (!nflag) {
            PrintCombinedHeading();
          }
          std::vector<std::string> return_strings;
          if (!config_manager.ReadConfigLike(key_arg, true, &return_strings)) {
            std::cerr << "ERROR: failed to read config" << std::endl;
            exit(EXIT_FAILURE);
          }
          // Print all the result rows
          PrintRows(return_strings);
        } else if (lflag) {
          // if read --like
          if (!nflag) {
            PrintShortHeading();
          }
          std::vector<std::string> return_strings;
          if (!config_manager.ReadConfigLike(key_arg, false, &return_strings)) {
            std::cerr << "ERROR: failed to read config" << std::endl;
            exit(EXIT_FAILURE);
          }
          // Print all the result rows
          PrintRows(return_strings);
        } else {
          if (aflag) {
            if (!nflag) {
              // Print column headings
              PrintCombinedHeading();
            }
            std::string read_string;
            // Read the config verbose
            if (config_manager.ReadConfig(&read_string, key_arg, true)) {
              std::cout << read_string << std::endl;
            } else {
              std::cerr << "ERROR: No matching key or missing meta_id"
                        << std::endl;
            }
          } else {
            std::string read_string;
            if (config_manager.ReadConfig(&read_string, key_arg)) {
              adk::config::ConfigValueType config_type;
              // Check to see if the config type is enum

              if (!config_manager.GetConfigValueType(key_arg, &config_type)) {
                std::cerr << "ERROR: Unable to read value type" << std::endl;
                exit(EXIT_FAILURE);
              }
              if (config_type == adk::config::kEnum) {
                std::string constraint_string;
                int meta_id;
                // Get the meta_id for the config item in order to get the enum
                // JSON string
                if (!config_manager.GetConfigMetaID(key_arg, &meta_id)) {
                  std::cerr << "ERROR: Unable to get config meta id"
                            << std::endl;
                  exit(EXIT_FAILURE);
                }
                // Get the JSON constraint string to check get the enum item
                // name
                if (!config_manager.GetConstraintByID(meta_id,
                                                      &constraint_string)) {
                  std::cerr << "ERROR: Unable to get constraint string for enum"
                            << std::endl;
                  exit(EXIT_FAILURE);
                }
                // Get a map of the enum item name strings to their int value
                std::map<std::string, int> enum_map;
                if (!config_manager.GetEnum(constraint_string, &enum_map)) {
                  std::cerr << "ERROR: Unable to get enum item strings and "
                               "values from json constraint"
                            << std::endl;
                  exit(EXIT_FAILURE);
                }
                int enum_int;
                // convert the stored int string to int
                try {
                  enum_int = std::stoi(read_string);
                } catch (const std::invalid_argument& ia) {
                  std::cout << "ERROR: Stored enum value is not an int"
                            << std::endl;
                  exit(EXIT_FAILURE);
                } catch (const std::out_of_range& oa) {
                  std::cerr << "ERROR: Integer out of range" << std::endl;
                  exit(EXIT_FAILURE);
                }
                // For each enum check if the value matches and get the enum
                // item string for the first value to match
                for (auto const& e : enum_map) {
                  if (e.second == enum_int) {
                    // Output the item name string for the enum
                    std::cout << e.first << std::endl;
                  }
                }
              } else {
                std::cout << read_string << std::endl;
              }
            } else {
              std::cerr << "ERROR: No matching key found" << std::endl;
            }
          }
        }
      }
    } else if (command_string == "write") {
      // Write must be given a key
      if (argv[optind + 1] == NULL) {
        std::cerr << "ERROR: no key given to write to" << std::endl;
      } else if (argv[optind + 2] == NULL) {
        // Write must be given a value
        std::cerr << "ERROR: no value given to write" << std::endl;
      } else {
        // Get key and value strings
        std::string key_arg = argv[(optind + 1)];
        std::string value_arg = argv[(optind + 2)];
        // If key is read only AND --ignore is not used. disallow writing
        if (config_manager.KeyIsReadOnly(key_arg) && !iflag) {
          std::cerr << "ERROR: key is read only" << std::endl;
        } else {
          int meta_id;
          // Get the config meta id to verify constraints
          if (!config_manager.GetConfigMetaID(key_arg, &meta_id)) {
            std::cerr << "ERROR: Failed to get key meta ID" << std::endl;
          } else {
            adk::config::ConfigValueType config_type;
            // Check to see if the config type is enum
            if (!config_manager.GetConfigValueType(key_arg, &config_type)) {
              std::cerr << "ERROR: Unable to read value type" << std::endl;
              exit(EXIT_FAILURE);
            }
            if (config_type == adk::config::kEnum) {
              // If it is enum check if it is a string or number. i.e. Is the
              // user editing the value by using the name or the int value
              char* p;
              strtol(value_arg.c_str(), &p, 10);
              if (*p) {
                std::string constraint_string;
                // Get the JSON constraint string to check get the enum item
                // name
                if (!config_manager.GetConstraintByID(meta_id,
                                                      &constraint_string)) {
                  std::cerr << "ERROR: Unable to get constraint string for enum"
                            << std::endl;
                  exit(EXIT_FAILURE);
                }
                // Get a map of the enum item name strings to their int value
                std::map<std::string, int> enum_map;
                if (!config_manager.GetEnum(constraint_string, &enum_map)) {
                  std::cerr << "ERROR: Unable to get enum item strings and "
                               "values from json constraint"
                            << std::endl;
                  exit(EXIT_FAILURE);
                }
                // Check if the value exists in the enum map.
                if (enum_map.find(value_arg) == enum_map.end()) {
                  std::cerr
                      << "ERROR: No enum item name matching: " << value_arg
                      << std::endl;
                  exit(EXIT_FAILURE);
                }
                // Get the int value for the enum item string
                int enum_value = enum_map.at(value_arg);
                value_arg = std::to_string(enum_value);
              }
            }

            // If the meta id is 1 then ignore the constraints
            if (meta_id == 1) iflag = true;
            // Pass in cflag which is --no-create
            if (!config_manager.WriteConfig(key_arg, value_arg, iflag,
                                            !cflag)) {
              std::cerr << "ERROR: Write failed" << std::endl;
            }
          }
        }
      }
    } else if (command_string == "add") {
      // Add requires a key
      if (argv[optind + 1] == NULL) {
        std::cerr << "ERROR: no key given to write to" << std::endl;
      } else if (argv[optind + 2] == NULL) {
        // Add requires a value
        std::cerr << "ERROR: no value given to write" << std::endl;
      } else {
        int constraint_id;
        // Get key and value strings
        std::string key_arg = argv[(optind + 1)];
        std::string value_arg = argv[(optind + 2)];
        // Inform user if the key already exists
        if (config_manager.KeyExists(key_arg)) {
          std::cerr << "ERROR: KEY already exists" << std::endl;
        } else {
          if (mflag && moptions != 1) {
            // Add the config with a specific meta-id. Do NOT ignore constraints
            constraint_id = moptions;
            config_manager.AddConfig(key_arg, value_arg, false, constraint_id);
          } else {
            // Add the config with the default constraint of 1
            config_manager.AddConfig(key_arg, value_arg);
          }
          // Add a default value to a key
          if (dflag) {
            int meta_id;
            config_manager.GetConfigMetaID(key_arg, &meta_id);
            if (meta_id == 1) {
              config_manager.AddDefaultValue(key_arg, doptions, true);
            } else {
              config_manager.AddDefaultValue(key_arg, doptions);
            }
          }
        }
      }
    } else if (command_string == "meta") {
      // --list option
      if (lflag) {
        if (!nflag) {
          PrintMetaHeadings();
        }
        std::vector<std::string> return_rows;
        if (!config_manager.ListConfigMeta(&return_rows)) {
          std::cerr << "ERROR: Listing meta failed" << std::endl;
        }
        PrintRows(return_rows);
      } else if (kflag) {
        // Get meta for a specific key
        if (!nflag) {
          PrintMetaHeadings();
        }
        std::string read_string;
        if (config_manager.GetMetaByKey(&read_string, koptions)) {
          std::cout << read_string << std::endl;
        } else {
          std::cout << "ERROR: No Meta found" << std::endl;
        }
      } else if (dflag) {
        // Delete a meta entry. Will fail if the meta is required by a key
        int delete_id;
        try {
          delete_id = std::stoi(doptions);
        } catch (const std::invalid_argument& ia) {
          std::cout << "ERROR: meta id must be an integer" << std::endl;
          exit(EXIT_FAILURE);
        } catch (const std::out_of_range& oa) {
          std::cerr << "ERROR: Integer out of range" << std::endl;
          exit(EXIT_FAILURE);
        }
        if (!config_manager.DeleteMetaByID(delete_id)) {
          std::cerr << "ERROR: Failed to delete meta entry" << std::endl;
        }
      } else if (mflag) {
        // Modify a constraint
        if (argv[optind + 1] == NULL) {
          std::cerr << "ERROR: no new constraint given" << std::endl;
        } else {
          std::string constraint_arg = argv[(optind + 1)];
          if (!config_manager.ModifyMetaByID(moptions, constraint_arg)) {
            std::cerr << "ERROR: Failed to modify meta entry" << std::endl;
          }
        }
      } else {
        // Add a new meta entry. Will validate the constraint string against the
        // defined JSON schema
        if (argv[optind + 1] == NULL) {
          std::cerr << "ERROR: no value type given" << std::endl;
        } else if (argv[optind + 2] == NULL) {
          std::cerr << "ERROR: no value constraint given" << std::endl;
        } else if (argv[optind + 3] == NULL) {
          std::cerr << "ERROR: no description given" << std::endl;
        } else {
          std::string value_type = argv[(optind + 1)];
          std::string value_constraint = argv[(optind + 2)];
          std::string description = argv[(optind + 3)];
          int meta_id;
          if (!config_manager.AddMeta(value_type, value_constraint, description,
                                      &meta_id)) {
            std::cerr << "ERROR: Failed to add meta" << std::endl;
          } else {
            std::cout << meta_id << std::endl;
          }
        }
      }
    } else if (command_string == "list") {
      std::vector<std::string> return_strings;
      if (aflag) {
        // Also show constraints of keys
        if (!nflag) {
          PrintCombinedHeading();
        }
        if (!config_manager.ListConfig(true, &return_strings)) {
          std::cerr << "ERROR: failed to list config" << std::endl;
          exit(EXIT_FAILURE);
        }
      } else {
        if (!nflag) {
          PrintShortHeading();
        }
        if (!config_manager.ListConfig(false, &return_strings)) {
          std::cerr << "ERROR: failed to list config" << std::endl;
          exit(EXIT_FAILURE);
        }
      }
      // Print all the rows of the results
      PrintRows(return_strings);
    } else if (command_string == "delete") {
      // Delete requires a key to delete
      if (argv[optind + 1] == NULL) {
        std::cerr << "ERROR: no value type given" << std::endl;
      }

      if (aflag) {
        // Also delete the constraint
        config_manager.DeleteConfigKey(argv[optind + 1], true);
      } else {
        config_manager.DeleteConfigKey(argv[optind + 1], false);
      }
    } else if (command_string == "config") {
      if (vflag) {
        // --version option
        std::string database_version_string;
        if (!config_manager.GetDatabaseVersion(&database_version_string)) {
          std::cerr << "ERROR: Can't read database version" << std::endl;
        }
        std::cout << "DATABASE FILE: " + database_location << std::endl;
        std::cout << "VERSION: " + database_version_string << std::endl;
      }
    } else if (command_string == "reset") {
      if (argv[optind + 1] == NULL) {
        // If no key is provided then reset all keys in the table
        if (aflag) {
          config_manager.RestoreDefault();
        } else {
          std::cerr << "ERROR: no value type given" << std::endl;
        }
      } else {
        // If a key is probided then only reset that key to its default
        std::string key_value = argv[optind + 1];
        if (!config_manager.RestoreDefault(key_value)) {
          std::cerr << "ERROR: key not found" << std::endl;
        }
      }
    } else {
      // The provided command did not match a known command
      std::cout << "adkcfg: \'" + command_string +
                       "\' is not a adkcfg command. See 'adkcfg --help'."
                << std::endl;
    }
  }
}

void PrintHelp(void) {
  using std::cout;
  using std::endl;
  using std::setw;
  using std::left;
  using std::right;

  cout << "Usage: adkcfg [--help] <command> [OPTION]" << endl << endl;
  cout << "These are the commands used to access and modify"
          " data in the ADK config database"
       << endl
       << endl;

  cout << "    " << setw(10) << left << "list" << setw(20)
       << "list all entries in the configuration" << endl;

  cout << "    " << setw(10) << left << "read" << setw(20)
       << "read the value of a key" << endl;

  cout << "    " << setw(10) << left << "write" << setw(20)
       << "write a value to an existing key" << endl;

  cout << "    " << setw(10) << left << "add" << setw(20)
       << "add a new key value pair to the configuration" << endl;

  cout << "    " << setw(10) << left << "meta" << setw(20)
       << "add meta information. Use [-l] to list all" << endl;

  cout << "    " << setw(10) << left << "config" << setw(20)
       << "change adkcfg configuration settings" << endl;

  cout << "    " << setw(10) << left << "delete" << setw(20)
       << "delete configuration key value" << endl;

  cout << "    " << setw(10) << left << "reset" << setw(20)
       << "reset key to default value. use [-a] to reset all keys" << endl
       << endl;
  cout << "OPTIONS:" << endl;

  cout << "    " << setw(30) << left << "--like, -l" << setw(20)
       << "read like a key" << endl;
  cout << "    " << setw(30) << left << "--file [FILE_PATH], -f" << setw(20)
       << "specify a FILE_PATH to the config file" << endl;
  cout << "    " << setw(30) << left << "--all, -a" << setw(20)
       << "Show verbose information" << endl;
  cout << "    " << setw(30) << left << "--key [KEY], -k" << setw(20)
       << "Specify key to read meta for" << endl;
  cout << "    " << setw(30) << left << "--list" << setw(20)
       << "used with meta to list all meta entries" << endl;
  cout << "    " << setw(30) << left << "--version" << setw(20)
       << "config --version to get config file version" << endl;
  cout << "    " << setw(30) << left << "--ignore" << setw(20)
       << "ignore constraints and read only flags" << endl;
  cout << "    " << setw(30) << left << "--default [VALUE]" << setw(20)
       << "specify default value" << endl;
  cout << "    " << setw(30) << left << "--meta [META_ID]" << setw(20)
       << "specify meta value" << endl;
  cout << "    " << setw(30) << left << "--delete [META_ID]" << setw(20)
       << "used with meta to delete a meta entry" << endl;
  cout << "    " << setw(30) << left << "--modify [ID] [CONSTRAINT]" << setw(20)
       << "used with meta to modify constraint" << endl;
  cout << "    " << setw(30) << left << "--no-heading" << setw(20)
       << "don't output headings" << endl;
  cout << endl;

  cout << "Set the environment variable ADKDB_FILE to specify a config file"
          " location."
       << endl;
  cout << "If no environment variable is set. adkcfg will search in the"
          " current directory"
       << endl;
  cout << endl;
}

void PrintVerboseHelp(void) {
  using std::cout;
  using std::endl;
  using std::setw;
  using std::left;
  using std::right;

  cout << "Usage: adkcfg [--help] <command> [OPTION]" << endl << endl;

  cout << "adkcfg is used to modify configuration data on the target "
          "platform."
       << endl
       << endl;
  cout << "It can be used to read, modify, add and delete key-value pairs"
       << endl
       << "as well as making modifications "
          "to the meta information associated"
       << endl
       << endl;

  cout << "adkcfg will raise warnings if the user attempts to modify a "
          "read-only key or modifies a value outside it's constraints"
       << endl
       << endl;

  cout << "-------------------------------------------------------------"
       << endl
       << endl;

  cout << "These are the COMMANDS used to access and modify"
          " data in the ADK config database"
       << endl
       << endl;

  cout << "    " << setw(20) << left << "list" << setw(20)
       << "list all entries in the configuration" << endl;

  cout << "    " << setw(20) << left << "read" << setw(20)
       << "read the value of a key" << endl;

  cout << "    " << setw(20) << left << "write" << setw(20)
       << "write a value to an existing key" << endl;

  cout << "    " << setw(20) << left << "add" << setw(20)
       << "add a new key value pair to the configuration" << endl;

  cout << "    " << setw(20) << left << "meta" << setw(20)
       << "add meta information. Use [-l] to list all" << endl;

  cout << "    " << setw(20) << left << "config" << setw(20)
       << "change adkcfg configuration settings" << endl;

  cout << "    " << setw(20) << left << "delete" << setw(20)
       << "delete configuration key value" << endl;

  cout << "    " << setw(20) << left << "reset" << setw(20)
       << "reset key to default value. use [-a] to reset all keys" << endl
       << endl;
  cout << "Options:" << endl << endl;

  cout << "    " << setw(30) << left << "--like, -l" << setw(20)
       << "read like a key" << endl;
  cout << "    " << setw(30) << left << "--file [FILE_PATH], -f" << setw(20)
       << "specify a FILE_PATH to the config file" << endl;
  cout << "    " << setw(30) << left << "--all, -a" << setw(20)
       << "Show verbose information" << endl;
  cout << "    " << setw(30) << left << "--key [KEY], -k" << setw(20)
       << "Specify key to read meta for" << endl;
  cout << "    " << setw(30) << left << "--list" << setw(20)
       << "used with meta to list all meta entries" << endl;
  cout << "    " << setw(30) << left << "--version" << setw(20)
       << "config --version to get config file version" << endl;
  cout << "    " << setw(30) << left << "--ignore" << setw(20)
       << "ignore constraints and read only flags" << endl;
  cout << "    " << setw(30) << left << "--default [VALUE]" << setw(20)
       << "specify default value" << endl;
  cout << "    " << setw(30) << left << "--meta [META_ID]" << setw(20)
       << "specify meta value" << endl;
  cout << "    " << setw(30) << left << "--delete [META_ID]" << setw(20)
       << "used with meta to delete a meta entry" << endl;
  cout << "    " << setw(30) << left << "--modify [ID] [CONSTRAINT]" << setw(20)
       << "used with meta to modify constraint" << endl;
  cout << "    " << setw(30) << left << "--no-heading" << setw(20)
       << "don't output headings" << endl;
  cout << endl;

  cout << "Environment:" << endl << endl;
  cout << "adkcfg is designed to be run on a target platform APQ8017 or "
          "APQ8009. "
       << endl;

  cout << "Set the environment variable ADKDB_FILE to specify a config file"
          " location."
       << endl;
  cout << "If no environment variable is set. adkcfg will search in the"
          " current directory"
       << endl;
  cout << endl;
  cout << "Examples:" << endl << endl;

  cout << "Here are some common commands that can be used:" << endl << endl;
  cout << "    " << setw(30) << left << "adkcfg list" << setw(30)
       << "List all key value pairs in database" << endl;

  cout << "    " << setw(30) << left << "adkcfg read KEY" << setw(30)
       << "Read the value of a KEY" << endl;

  cout << "    " << setw(30) << left << "adkcfg read PREFIX -l" << setw(30)
       << "Read all key-value pairs that contain the PREFIX" << endl;

  cout << "    " << setw(30) << left << "adkcfg write KEY VALUE" << setw(30)
       << "Write VALUE to KEY. use --ignore to overite read-only or"
          " constraints"
       << endl;

  cout << "    " << setw(30) << left << "adkcfg add KEY VALUE" << setw(30)
       << "Add a new key-value pair. use --meta to specify meta constraint ID"
       << endl;

  cout << "    " << setw(30) << left << "adkcfg delete KEY" << setw(30)
       << "Delete KEY from configuration" << endl;

  cout << "    " << setw(30) << left << "adkcfg meta --list" << setw(30)
       << "List all meta data" << endl;

  cout << "    " << setw(30) << left << "adkcfg meta --key KEY" << setw(30)
       << "Read meta data for KEY" << endl;

  cout << "The schema for the configuration system is as follows:" << endl
       << endl;
  cout << "The main functionality is achieved using Key-Value pairs. The key "
          "must be unique."
       << endl
       << endl;
  cout << "There are two tables used for the configuration data:" << endl
       << endl;

  cout << "-------------------------------------------------------------"
       << endl
       << endl;

  cout << "config_table: Contains key-value pairs, access"
          " flags and a default value"
       << endl
       << "it also contains a row id for"
          " the meta data associated with the key-value pair"
       << endl
       << endl;
  cout << "Fields: KEY | VALUE | DEFAULT VALUE |"
          " READ-ONLY | INTERNAL-ONLY | META ID"
       << endl
       << endl;

  cout << "-------------------------------------------------------------"
       << endl
       << endl;

  cout << "meta_table: contains meta informatation"
          " such as value type and constraints"
       << endl
       << endl;
  cout << "Fields: VALUE TYPE | VALUE CONSTRAINTS | DESCRIPTION" << endl
       << endl;

  cout << "-------------------------------------------------------------"
       << endl
       << endl;

  cout << "The constraints in the meta information is stored using JSON "
          "format."
          " There are three types of constraint: range, list and enum. A "
          "constraint"
          " can constain multiple ranges: i.e 1-10 and 30-50. A range SHALL "
          "have"
          " a min and a max key."
       << endl
       << endl;

  cout << "example list json format with strings:"
          " {\"list\":[\"string1\",\"string2\",\"string3\"]}"
       << endl
       << endl;

  cout << "example list with ints: {\"list\":[1,2,3]}" << endl << endl;

  cout << "example single range:"
          " {\"range\":[{\"min\":1,\"max\":10}]}"
       << endl
       << endl;

  cout << "example multi range:"
          " {\"range\":[{\"min\":1,\"max\":10},{\"min\":30,\"max\":50}]}"
       << endl
       << endl;

  cout << "example multi range:"
          " {\"enum\":{\"FAILURE\":1,\"WARNING\":2,\"SUCCESS\":3}}"
       << endl
       << endl;
}

void PrintMetaHeadings(void) {
  std::cout << "ID | Value Type | Value Constraint | Description" << std::endl
            << std::endl;
}

void PrintLongHeading(void) {
  std::cout << "Key | Value | Default | Read-Only | Internal" << std::endl
            << std::endl;
}

void PrintCombinedHeading(void) {
  std::cout << "Key | Value | Default | Read-Only | Internal | "
               "Value Type | Constraint Value | Description"
            << std::endl
            << std::endl;
}

void PrintShortHeading(void) {
  std::cout << "Key | Value" << std::endl << std::endl;
}

void PrintVersionHeader(void) {
  std::cout << "adkcfg version " << ADKCFG_VERSION << std::endl;
}

bool PrintRows(std::vector<std::string> row_vector) {
  // Return false if there are no rows in the list
  if (row_vector.size() == 0) {
    return false;
  }
  // For each of the rows print the row in a new line
  for (auto row : row_vector) {
    std::cout << row << std::endl;
  }
  return true;
}
