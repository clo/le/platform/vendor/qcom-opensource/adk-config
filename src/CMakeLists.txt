
cmake_minimum_required (VERSION 3.1)

include(GNUInstallDirs)

set(libsrc
  adk_cfg_db_manager.cc
  config.cc
)
add_library(
  adk-config SHARED
  ${libsrc}
)
add_definitions(-DCONFIG_PATH="${CONFIG_DEFAULT_PATH}")


target_link_libraries(
  adk-config
  ${CMAKE_THREAD_LIBS_INIT}
  ${CMAKE_DL_LIBS}
  ${SQLITE3_LIBRARIES}
  ${JSONC_LIBRARIES}
)

install(
   TARGETS adk-config
   LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
   PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
add_executable(
  adkcfg
  adkcfg.cc
  adk_cfg_db_manager.cc
)

target_link_libraries(
  adkcfg
  ${CMAKE_THREAD_LIBS_INIT}
  ${CMAKE_DL_LIBS}
  ${SQLITE3_LIBRARIES}
  ${JSONC_LIBRARIES}
)

install(
  TARGETS adkcfg
  DESTINATION ${CMAKE_INSTALL_BINDIR}
)
