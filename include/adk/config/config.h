/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INCLUDE_ADK_CONFIG_CONFIG_H_
#define INCLUDE_ADK_CONFIG_CONFIG_H_

#include <stdint.h>
#include <memory>
#include <string>
#include <vector>

namespace adk {
namespace config {
/**
  *  @class Config
  *
  *  @brief Config class which enables applications to read and write to a
  * configuration file using the estabilished schema.
  *
  */
class Config final {
 public:
  // Disallowing copy and assignment operations
  Config(const Config&) = delete;
  Config& operator=(const Config&) = delete;

  /**
    * @brief Default destructor.
    *
    */
  ~Config();
  /**
    * @brief Read a boolean value.
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key
    * @param[in] key Key to read from
    */
  bool Read(bool* value, const std::string& key) const;
  /**
    * @brief Read a boolean value at a given index.
    *
    * @note The key that will be read is in the form <key>.<index>.<subkey>
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key
    * @param[in] index Index to read from
    * @param[in] key First component of the key to read.
    * @param[in] subkey The second component of the key to read
    */
  bool Read(bool* value, int index, const std::string& key,
            const std::string& subkey) const;
  /**
    * @brief Read an int value.
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key
    * @param[in] key Key to read from
    */
  bool Read(int32_t* value, const std::string& key) const;
  /**
    * @brief Read a int value at a given index.
    *
    * @note The key that will be read is in the form <key>.<index>.<subkey>
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key
    * @param[in] index Index to read from
    * @param[in] key First component of the key to read
    * @param[in] subkey The second component of the key to read
    */
  bool Read(int32_t* value, int index, const std::string& key,
            const std::string& subkey) const;
  /**
    * @brief Read a double value.
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key
    * @param[in] key Key to read from
    */
  bool Read(double* value, const std::string& key) const;
  /**
    * @brief Read a double value at a given index.
    *
    * @note The key that will be read is in the form <key>.<index>.<subkey>
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key.
    * @param[in] index Index to read from
    * @param[in] key First component of the key to read.
    * @param[in] subkey The second component of the key to read.
    */
  bool Read(double* value, int index, const std::string& key,
            const std::string& subkey) const;
  /**
    * @brief Read a string value.
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key.
    */
  bool Read(std::string* value, const std::string& key) const;
  /**
    * @brief Read a string value at a given index.
    *
    * @note The key that will be read is in the form <key>.<index>.<subkey>
    *
    * @returns True If the read was successful
    * @param[out] value The value read from the config key.
    * @param[in] index Index to read from
    * @param[in] key First component of the key to read.
    * @param[in] subkey The second component of the key to read.
    */
  bool Read(std::string* value, int index, const std::string& key,
            const std::string& subkey) const;
  /**
    * @brief write a boolean value to a key.
    *
    * @returns True If the write was successful
    * @param[in] value value to write to database
    * @param[in] key key to write to
    */
  bool Write(bool value, const std::string& key, bool create = false);
  /**
    * @brief write an int value to a key. This will overwrite the key if it
    * exists, regardless of any constraints stored in the meta for that config
    * key. If the key does not exist then a new one will be created with the
    * default meta information.
    *
    * @returns True If the write was successful
    * @param[in] value Value to write to database
    * @param[in] key Key to write to
    */
  bool Write(int32_t value, const std::string& key, bool create = false);
  /**
    * @brief write a null terminated c-style string to to a key. This will
    * overwrite the key if it
    * exists, regardless of any constraints stored in the meta for that config
    * key. If the key does not exist then a new one will be created with the
    * default meta information
    *
    * @returns True If the write was successful
    * @param[in] value Value to write to database
    * @param[in] key Key to write to
    */
  bool Write(const char* value, const std::string& key, bool create = false);
  /**
    * @brief Write a double value to a key. This will overwrite the key if it
    * exists, regardless of any constraints stored in the meta for that config
    * key. If the key does not exist then a new one will be created with the
    * default meta information.
    *
    * @returns True If the write was successful
    * @param[in] value Value to write to database
    * @param[in] key Key to write to
    */
  bool Write(double value, const std::string& key, bool create = false);
  /**
    * @brief Write a string value to a key. This will overwrite the key if it
    * exists, regardless of any constraints stored in the meta for that config
    * key. If the key does not exist then a new one will be created with the
    * default meta information.
    *
    * @returns True If the write was successful
    * @param[in] value Value to write to database
    * @param[in] key Key to write to
    */
  bool Write(const std::string& value, const std::string& key,
             bool create = false);
  /**
    * @brief Restore a key to its default value. This will overwrite the key if
    * it exists, regardless of any constraints stored in the meta for that
    * config key. If the key does not exist then a new one will be created with
    * the default meta information.
    *
    * @returns True If the restore was successful
    * @param[in] key Key to restore
    */
  bool Restore(const std::string& key);

  /**
    * @brief Returns whether the key exists or not.
    *
    * @returns True If the key exists
    * @param[in] key The key to check if exists
    */
  bool KeyExists(const std::string& key);


  /**
    * @brief Factory method for creating a configuration object. This will return
    * a valid valid pointer to a Config Object if the connection to the database
    * is successful
    *
    * @returns valid pointer to a Config Object if database initialisation is
    * successful
    * otherwise will return nullptr
    * @param[in] database_file Full database file path to open.
    */
  static std::unique_ptr<Config> Create(const std::string& database_file);

 private:
  /**
    * @brief Private Constructor - use the Create() public method to create a Config object
    *
    * @param[in] dbFilename The filename of the database to create a connection to
    * to.
    */
  explicit Config(const std::string& dbFilename);
  /**
    * @brief Initialise the ConfigManager object. This must be called before
    * calling any other methods
    *
    * @returns True If the initialisation was successful
    */
  bool InitialiseManager();
  /**
    * @struct Private
    * @brief Private data structure containing all private data variables.
    */
  struct Private;
  /**
    * @variable self_
    * @brief Pointer to the private structure.
    */
  std::unique_ptr<Private> self_;
};

}  // namespace config
}  // namespace adk
#endif  // INCLUDE_ADK_CONFIG_CONFIG_H_
