/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "configdefs.h"

#include <chrono>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>

#include <adk/config/config.h>

/*
  see examples/example.adkschema.txt for the values and names in the database
  database name is: example.db

  $ cd build
  $ sqlite3 example.db < test/example.adkschema.txt
*/

using std::cout;
using std::endl;
using std::string;

int main(int argc, char* argv[]) {
  cout << "adk-library test\n";

  int pwr_pio_value;
  if (config::power::kGpio.read(&pwr_pio_value)) {
    cout << "pwr_pio_value = " << pwr_pio_value << endl;
  } else {
    cout << "pwr_pio_value failed to read" << endl;
  }

  bool b_gpio_debounce;
  if (config::gpio::kDebounce.read(&b_gpio_debounce)) {
    cout << "b_gpio_debounce = " << b_gpio_debounce << endl;
  } else {
    cout << "b_gpio_debounce failed to read" << endl;
  }

  string s_gpio_debounce;
  if (config::gpio::kDebounce.read(&s_gpio_debounce)) {
    cout << "s_gpio_debounce = " << s_gpio_debounce << endl;
  } else {
    cout << "s_gpio_debounce failed to read" << endl;
  }

  string ui_direction;
  if (config::ui::kDirection.read(&ui_direction)) {
    cout << "ui_direction = " << ui_direction << endl;
  } else {
    cout << "ui_direction failed to read" << endl;
  }

  // reading a list example
  int list_count;
  if (config::ui::kButtonListCount.read(&list_count)) {
    cout << "list count = " << list_count << endl;
  } else {
    cout << "list count failed to read" << endl;
    list_count = 0;
  }

  //  for (int i = 0; i < list_count; i++) {
  for (int i = 0; i < list_count; i++) {
    int button_gpio;
    string button_name;
    string button_event;

    if (!config::ui::kButtonListGpio.read(&button_gpio, i)) {
      cout << "button_gpio failed to read" << endl;
    }
    if (!config::ui::kButtonListName.read(&button_name, i)) {
      cout << "button_name failed to read" << endl;
    }
    if (!config::ui::kButtonListEvent.read(&button_event, i)) {
      cout << "button_event failed to read" << endl;
    }

    cout << i << ":" << endl;
    cout << "\tgpio: " << button_gpio << endl;
    cout << "\tname: " << button_name << endl;
    cout << "\tevent: " << button_event << endl;
  }

  // create a new key/value example, write the time (remove the newline)
  const adk::config::Config kNewKey = Config(kConfigFile, "test.system_time");
  const time_t ctt = time(0);
  std::string the_time = asctime(localtime(&ctt));
  std::string new_key;

  the_time = the_time.substr(0, the_time.find_last_of('\n'));

  // check if the key already exists
  if (kNewKey.read(&new_key)) {
    cout << "new key exists, value = " << new_key << endl;
  } else {
    cout << "new key does not exist" << endl;
  }

  if (kNewKey.write(the_time)) {
    cout << "successfully wrote the time and date to a key" << endl;
  } else {
    cout << "failed to write the time and date to a key" << endl;
  }

  if (kNewKey.read(&new_key)) {
    cout << "key value = " << new_key << endl;
  } else {
    cout << "new_key failed to read" << endl;
  }

  return 0;
}
