/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <gtest/gtest.h>
#include <adk/config/config.h>

class BaseManagerTest : public ::testing::Test {
 public:
  BaseManagerTest():config_("adk.core.db") {}
 protected:
  virtual void SetUp() {
    EXPECT_TRUE(config_.InitialiseManager());
  }
  adk::config::Config config_;
};


class ReadTest : public BaseManagerTest {};

class WriteTest : public BaseManagerTest {};

class AdditionalFeaturesTest : public BaseManagerTest {};

class RangeTest : public BaseManagerTest {};

TEST(FactoryMethodTest, Create) {
  EXPECT_TRUE((adk::config::Create("adk.core.db")) != nullptr);
}

TEST(FactoryMethodTest, InvalidDatabase) {
  EXPECT_FALSE((adk::config::Create("foo.db")) != nullptr);
}

TEST_F(ReadTest, ReadInt) {
  int read_int;
  EXPECT_TRUE(config_.Read(&read_int, "readtest.readint"));
  EXPECT_EQ(10, read_int);
}

TEST_F(ReadTest, ReadIntMalformed) {
  int read_int;
  EXPECT_FALSE(config_.Read(&read_int, "readtest.readintmalformed"));
}

TEST_F(ReadTest, ReadBool) {
  bool read_bool;
  EXPECT_TRUE(config_.Read(&read_bool, "readtest.bool"));
  EXPECT_TRUE(read_bool);
}

TEST_F(ReadTest, ReadBoolMalformed) {
  bool read_bool;
  EXPECT_FALSE(config_.Read(&read_bool, "readtest.readboolmalformed"));
}

TEST_F(ReadTest, ReadString) {
  std::string read_string;
  EXPECT_TRUE(config_.Read(&read_string, "readtest.readstring"));
  EXPECT_EQ("test_string", read_string);
}

TEST_F(ReadTest, ReadIntAsString) {
  std::string read_string;
  EXPECT_TRUE(config_.Read(&read_string, "readtest.readintasstring"));
  EXPECT_EQ("23", read_string);
}

TEST_F(ReadTest, ReadDouble) {
  double read_double;
  EXPECT_TRUE(config_.Read(&read_double, "readtest.readdouble"));
  EXPECT_DOUBLE_EQ(4.2, read_double);
}

TEST_F(ReadTest, ReadDoubleMalformed) {
  double read_double;
  EXPECT_FALSE(config_.Read(&read_double, "readtest.readdoublemalformed"));
}

TEST_F(ReadTest, ReadEmptyKey) {
  int read_int;
  EXPECT_FALSE(config_.Read(&read_int, ""));
}

TEST_F(ReadTest, ReadNonExistent) {
  int read_int;
  EXPECT_FALSE(config_.Read(&read_int, "test.NotExist"));
}

TEST_F(WriteTest, WriteBool) {
  bool bool_test;
  EXPECT_TRUE(config_.Read(&bool_test, "writetest.writebool"));

  EXPECT_TRUE(config_.Write(!bool_test, "writetest.writebool"));

  bool bool_retest;
  EXPECT_TRUE(config_.Read(&bool_retest, "writetest.writebool"));

  EXPECT_TRUE(bool_retest == !bool_test);

  EXPECT_TRUE(config_.Restore("writetest.writebool"));
}

TEST_F(WriteTest, WriteString) {
  std::string string_test;
  EXPECT_TRUE(config_.Read(&string_test, "writetest.writestring"));

  EXPECT_TRUE(config_.Write(string_test + "test", "writetest.writestring"));

  std::string string_retest;
  EXPECT_TRUE(config_.Read(&string_retest, "writetest.writestring"));

  EXPECT_EQ((string_test + "test"), string_retest);

  EXPECT_TRUE(config_.Restore("writetest.writestring"));
}

TEST_F(WriteTest, WriteDouble) {
  double double_test;
  EXPECT_TRUE(config_.Read(&double_test, "writetest.writedouble"));

  EXPECT_TRUE(config_.Write(double_test + 1.5, "writetest.writedouble"));

  double double_retest;
  EXPECT_TRUE(config_.Read(&double_retest, "writetest.writedouble"));

  EXPECT_DOUBLE_EQ(double_test + 1.5, double_retest);

  EXPECT_TRUE(config_.Restore("writetest.writedouble"));
}

TEST_F(WriteTest, WriteInt) {
  int int_test;
  EXPECT_TRUE(config_.Read(&int_test, "writetest.writeint"));

  EXPECT_TRUE(config_.Write(int_test + 5, "writetest.writeint"));

  int int_retest;
  EXPECT_TRUE(config_.Read(&int_retest, "writetest.writeint"));

  EXPECT_EQ((int_test + 5), int_retest);

  EXPECT_TRUE(config_.Restore("writetest.writeint"));
}
TEST_F(AdditionalFeaturesTest, Restore) {
  EXPECT_TRUE(config_.Write(123, "additionalfeaturestest.restore"));
  EXPECT_TRUE(config_.Restore("additionalfeaturestest.restore"));
  int default_value;
  EXPECT_TRUE(config_.Read(&default_value, "additionalfeaturestest.restore"));
  EXPECT_NE(123, default_value);
  EXPECT_EQ(10, default_value);
}
TEST_F(AdditionalFeaturesTest, KeyExists) {
  EXPECT_TRUE(config_.KeyExists("additionalfeaturestest.keyexists"));
}
TEST_F(RangeTest, ReadInt) {
  int read_int;
  EXPECT_FALSE(config_.Read(&read_int, "rangetest.readint"));
}

TEST_F(RangeTest, ReadDouble) {
  double read_int;
  EXPECT_FALSE(config_.Read(&read_int, "rangetest.readdouble"));
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
