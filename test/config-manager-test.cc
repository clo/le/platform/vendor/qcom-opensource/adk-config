/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <gtest/gtest.h>
#include "adk_cfg_db_manager.h"
using adk::config::ConfigManager;
TEST(ConfigIOTest, Initialise) {
  ConfigManager config;
  ASSERT_TRUE(config.InitialiseManager("adk.core.db"));
}

class BaseManagerTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
    EXPECT_TRUE(config_manager_.InitialiseManager("adk.core.db"));
  }

  virtual void TearDown() { /* EXPECT_TRUE(config_manager_.RestoreDefault()); */
  }

  ConfigManager config_manager_;
};

class ReadingDatabaseTest : public BaseManagerTest {};

class GeneralTest : public BaseManagerTest {};

class InvalidInputTest : public BaseManagerTest {};

class ModifyDatabaseTest : public BaseManagerTest {};

TEST_F(ReadingDatabaseTest, GetVersionTest) {
  std::string mock;
  ASSERT_TRUE(config_manager_.GetDatabaseVersion(&mock));
}

TEST_F(ReadingDatabaseTest, ReadintValue) {
  std::string key = "readingdatabasetest.readintvalue";
  std::string value;
  ASSERT_TRUE(config_manager_.ReadConfig(&value, key));
  ASSERT_STRCASEEQ("10", value.c_str());
}

TEST_F(ReadingDatabaseTest, ReaddoubleValue) {
  std::string key = "readingdatabasetest.readdoublevalue";
  std::string value;
  ASSERT_TRUE(config_manager_.ReadConfig(&value, key));
  ASSERT_STRCASEEQ("4.2", value.c_str());
}

TEST_F(ReadingDatabaseTest, ReadStrValue) {
  std::string key = "readingdatabasetest.readstrvalue";
  std::string value;
  ASSERT_TRUE(config_manager_.ReadConfig(&value, key));
  ASSERT_STRCASEEQ("ConfigManager_test_string", value.c_str());
}

TEST_F(ReadingDatabaseTest, ReadEnumValue) {
  // static const std::string arr[]=

  std::string key = "readingdatabasetest.readenumvalue";
  std::string value;
  ASSERT_TRUE(config_manager_.ReadConfig(&value, key));
  ASSERT_STRCASEEQ("2", value.c_str());
}

TEST_F(ReadingDatabaseTest, ReadConfigLike) {
  std::string key = "readingdatabasetest.readlike";
  std::vector<std::string> test_vector = {
      "readingdatabasetest.readliketest1|1",
      "readingdatabasetest.readliketest2|2",
      "readingdatabasetest.readliketest3|3"};
  std::vector<std::string> list_vector;
  EXPECT_TRUE(config_manager_.ReadConfigLike(key, false, &list_vector));
  ASSERT_TRUE(list_vector == test_vector);
}

TEST_F(ReadingDatabaseTest, GetConfigMetaID) {
  int meta_id;
  std::string test_key = "readingdatabasetest.getconfigmetaid";
  EXPECT_TRUE(config_manager_.GetConfigMetaID(test_key, &meta_id));
  EXPECT_EQ(5, meta_id);
}

TEST_F(ReadingDatabaseTest, GetMetaByKey) {
  std::string key = "readingdatabasetest.getmetabykey";
  std::string value;
  EXPECT_TRUE(config_manager_.GetMetaByKey(&value, key));
  ASSERT_STRCASEEQ("6|str|{\"list\":[\"hello\",\"world\"]}|readmetabykey test",
                   value.c_str());
}

TEST_F(ReadingDatabaseTest, MetaExistsByID) {
  EXPECT_TRUE(config_manager_.MetaExistsByID(1));
  EXPECT_FALSE(config_manager_.MetaExistsByID(1000));
}

TEST_F(ReadingDatabaseTest, GetConstraintByID) {
  int meta_id = 9;
  std::string json_string;
  std::string expected_json_string = "{\"list\":[1,10,100]}";
  EXPECT_TRUE(config_manager_.GetConstraintByID(9, &json_string));
  EXPECT_EQ(expected_json_string, json_string);
}
// TODO(abrosans): Write check constraint test

TEST_F(ModifyDatabaseTest, RestoreDefault) {
  std::string key = "modifydatabasetest.restoredefault";

  std::string value;
  EXPECT_TRUE(config_manager_.ReadConfig(&value, key));
  EXPECT_STRCASEEQ("2", value.c_str());
  std::string restored_value;
  EXPECT_TRUE(config_manager_.RestoreDefault(key));
  EXPECT_TRUE(config_manager_.ReadConfig(&restored_value, key));
  EXPECT_NE(restored_value, value);
  EXPECT_STRCASEEQ("10", restored_value.c_str());
  EXPECT_FALSE(config_manager_.RestoreDefault("KeyThatDoesNotExist"));
}

TEST_F(ModifyDatabaseTest, AddDefaultValue) {
  std::string key = "modifydatabasetest.adddefaultvalue";
  std::string restored_value;
  EXPECT_TRUE(config_manager_.RestoreDefault(key));
  EXPECT_TRUE(config_manager_.ReadConfig(&restored_value, key));
  EXPECT_TRUE(config_manager_.AddDefaultValue(key, std::to_string(20), true));
  std::string new_restored_value;
  EXPECT_TRUE(config_manager_.RestoreDefault(key));
  EXPECT_TRUE(config_manager_.ReadConfig(&new_restored_value, key));
  EXPECT_NE(new_restored_value, restored_value);
  EXPECT_STRCASEEQ("20", new_restored_value.c_str());
  EXPECT_FALSE(
      config_manager_.AddDefaultValue("KeyThatDoesNotExist", "test", true));
}

TEST_F(ModifyDatabaseTest, AddMeta) {
  int meta_row_id;
  EXPECT_TRUE(config_manager_.AddMeta(
      "int", "{\"list\":[1,2,3]}", "New meta for AddMeta test", &meta_row_id));
  EXPECT_EQ(10, meta_row_id);
}

TEST_F(ModifyDatabaseTest, WriteConfig) {
  std::string key = "modifydatabasetest.writeconfig";

  std::string value = "20";
  EXPECT_TRUE(config_manager_.WriteConfig(key, value, true, false));
  std::string new_value;
  EXPECT_TRUE(config_manager_.ReadConfig(&new_value, key));
  EXPECT_EQ(new_value, value);
  std::string value2 = "100";
  EXPECT_TRUE(config_manager_.WriteConfig(key, value2, false, false));
  std::string new_value2;
  EXPECT_TRUE(config_manager_.ReadConfig(&new_value2, key));
  EXPECT_EQ(new_value, value);
  std::string value3 = "25";
  EXPECT_FALSE(config_manager_.WriteConfig(key, value3, false, false));

  std::string key2 = "modifydatabasetest.writeconfig_new";
  std::string value4 = "20";
  EXPECT_FALSE(config_manager_.WriteConfig(key2, value4, true, false));
  EXPECT_TRUE(config_manager_.WriteConfig(key2, value4, true, true));
  std::string new_value3;
  EXPECT_TRUE(config_manager_.ReadConfig(&new_value3, key2));
  EXPECT_EQ(new_value3, value4);
}

TEST_F(ModifyDatabaseTest, DeleteConfigKey) {
  std::string key = "modifydatabasetest.deleteconfigkey";
  EXPECT_TRUE(config_manager_.DeleteConfigKey(key, false));
  EXPECT_FALSE(config_manager_.KeyExists(key));
}

TEST_F(ModifyDatabaseTest, DeleteMetaByID) {
  int meta_row_id;
  EXPECT_TRUE(config_manager_.AddMeta("int", "{\"list\":[1,2,3]}",
                                      "New meta for DeleteMetaByID test",
                                      &meta_row_id));
  EXPECT_TRUE(config_manager_.DeleteMetaByID(meta_row_id));
  EXPECT_FALSE(config_manager_.MetaExistsByID(meta_row_id));
}

TEST_F(ModifyDatabaseTest, ModifyMetaByID) {
  int meta_row_id = 8;
  std::string json_string = "{\"list\":[2,20,200]}";

  EXPECT_TRUE(config_manager_.ModifyMetaByID(meta_row_id, json_string));
  std::string new_json_string;
  EXPECT_TRUE(config_manager_.GetConstraintByID(meta_row_id, &new_json_string));
  EXPECT_EQ(json_string, new_json_string);
}

TEST_F(GeneralTest, ReadWriteRevert) {
  std::string key = "generaltest.readwriterevert";
  std::string value;

  EXPECT_TRUE(config_manager_.ReadConfig(&value, key));
  EXPECT_STRCASEEQ("foo", value.c_str());

  EXPECT_TRUE(config_manager_.WriteConfig(key, "bar", true, false));

  EXPECT_TRUE(config_manager_.ReadConfig(&value, key));
  EXPECT_STRCASEEQ("bar", value.c_str());

  EXPECT_TRUE(config_manager_.RestoreDefault(key));

  EXPECT_TRUE(config_manager_.ReadConfig(&value, key));
  EXPECT_STRCASEEQ("foo", value.c_str());
}

TEST_F(GeneralTest, ValidateConstraint) {
  EXPECT_TRUE(config_manager_.ValidateConstraint(R"({"range":[{"min":1,"max":10},{"min":30,"max":50}]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"range":{"min":1,"max":10}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"range":[{"max":10},{"min":30,"max":50}]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"range":[{"min":1},{"min":30,"max":50}]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"range":[{},{}]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"range":{}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"foo":{"bar":0}})"));

  EXPECT_TRUE(config_manager_.ValidateConstraint(R"({"list":[1,2,3]})"));
  EXPECT_TRUE(config_manager_.ValidateConstraint(R"({"list":[1.2,2.23,3.123]})"));
  EXPECT_TRUE(config_manager_.ValidateConstraint(R"({"list":["cat","dog","cow"]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"list":{"cat","dog","cow"}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"list":[{"rat"},{"dog"},{"cow"}]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"list":{"cat":4,"dog":1,"cow":2}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"list":{}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"list":[]})"));

  EXPECT_TRUE(config_manager_.ValidateConstraint(R"({"enum":{"FAILURE":1,"WARNING":2,"SUCCESS":3}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"enum"})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"enum":{"FAILURE","WARNING","SUCCESS"}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"enum":{"FAILURE":"2Fail","WARNING":"Fail","SUCCESS":"two"}})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"enum":["FAILURE":2,"WARNING":1]})"));
  EXPECT_FALSE(config_manager_.ValidateConstraint(R"({"enum":[]})"));
}

TEST_F(InvalidInputTest, ReadValue) {
  std::string key = "INVALID_KEY_TEST";
  std::string value;
  EXPECT_FALSE(config_manager_.ReadConfig(&value, key));

  EXPECT_FALSE(config_manager_.ReadConfig(&value, ""));
  EXPECT_FALSE(config_manager_.ReadConfig(&value, "NULL"));
  EXPECT_FALSE(
      config_manager_.ReadConfig(&value, "\"DROP TABLE database_meta_table\""));
  std::string mock;
  EXPECT_TRUE(config_manager_.GetDatabaseVersion(&mock));
}

TEST_F(InvalidInputTest, KeyExists) {
  ASSERT_FALSE(config_manager_.KeyExists("NULL"));

  ASSERT_FALSE(config_manager_.KeyExists("spaces in key"));
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
