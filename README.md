ADK Config
=============================
This repository contains the ADK Config library and the adkcfg CLI tool. See below for the README and how-to-guides.

ADKCFG CLI Tool
=============================
This is a command line tool for providing access to the ADK configuration database files

ADK Library - Configuration
=============================
A library for reading and writing to configuration files.

The configuration files are stored in an sqlite3 database with a pre-defined schema.

This library provides an interace to the functionality exposed by the schema. e.g. read/write, rollback, meta information, value constraints, description.


Requirements
------------

* Git
* CMake 3.1 or higher
* A C++ compiler, e.g., GCC,
* SQLITE3 Installed 
* JSON-C (libjson0-dev libjson0) Installed

How to build
-----------
The library can be build for a standard Linux machine when used outside of the Yocto system.
```
sudo apt-get install libsqlite3-dev libjson0-dev libjson-c-dev
```

```
mkdir build
cd build
cmake ../
make
sudo make install
```

How to test
-----------
adkcfg has a GTest written for the library and the underlying Config Manager.
To run the tests simply run `make test` after enabling tests in cmake:
```
cd build
cmake ../ -DWITH_TEST=ON
make test
```
The output should look like: 
```
Running tests...
Test project
    Start 1: Make_clean
1/4 Test #1: Make_clean .......................   Passed    0.02 sec
    Start 2: Make
2/4 Test #2: Make .............................   Passed   10.02 sec
    Start 3: ManagerTest
3/4 Test #3: ManagerTest ......................   Passed    2.11 sec
    Start 4: LibraryTest
4/4 Test #4: LibraryTest ......................   Passed    1.51 sec

100% tests passed, 0 tests failed out of 4

Total Test time (real) =  13.67 sec
```

Usage
=============================

See doxygen

Create the documentation using `cd doxygen; doxygen`
A set of HTML pages will be created in the doxygen directory. See index.html.

read
-----------

* create a Config object with the database filename.
* Initialise the config object by calling InitialiseManager(). This will return true if successful
* create a variable to store the value
* read the value of a key in to the variable 
* check the return value in case the key did not exist or could not be converted to the desired type

```
#include "adk/config/config.h"

adk::config::Config config_("adk.core.db");
if (config_.InitialiseManager()) {
        int pwr_pio_value;
        bool status;
        status = ConfigPwrGpio.read(&pwr_pio_value,"key");
}
```
read array
-----------

The following is an example of how to read a flattened array of structures.

One row should contain the number of items in the array, and then each group of rows contains the values for each member, with an incrementing index (starting from zero).

They should be in the form `<key>.index.<subkey>`.

For example, a simple representation of buttons:

```
struct button {
  int gpio;     // the gpio which the button is mapped to
  string name;  // name of the button
};

struct button_list {
  int count;                // how many buttons there are 
  struct button buttons[];  // an array of buttons
}

```
A list of structures is stored in a flattened array format of key/value pairs with an index that increments by one between each "structure":

```
ui.button_list.count|3
===
ui.button_list.0.gpio|5
ui.button_list.0.name|VOL_UP
---
ui.button_list.1.gpio|7
ui.button_list.1.name|PWR
---
ui.button_list.2.gpio|23
ui.button_list.2.name|PLAY_PAUSE
---
```

To read this back in to the array:
* Create a config variable for the count.
* Create config variables for the members in `<key>`,`<subkey>` form, and ignore the index.

```
#include "adk/config/config.h"

using adk::config::Config

const Config config_(CORE_CONFIG_FILENAME) = Config(CORE_CONFIG_FILENAME,   "ui.button_list.count");

const Config ConfigUiButtonListGpio  = Config(CORE_CONFIG_FILENAME,   "ui.button_list",   "gpio");
const Config ConfigUiButtonListName  = Config(CORE_CONFIG_FILENAME,   "ui.button_list",   "name");
```

To access the values:
* read the count
* loop through each entry by supplying the index in the `read()` member. This version of read() simply inserts `.<index>.` between the key and the subkey.

```
#include "adk/config/config.h"

using adk::config::Config;

std::unique_ptr<Config> config_ = adk::config::Config::Create(CORE_CONFIG_FILENAME);
if (config_ == nullptr) {
  return false;
}

int list_count;
config_.read(&list_count, "ui.button_list.count");

for (int i = 0; i < list_count; i++) {
  int button_gpio;
  std::string button_name;

  config_.read(&button_gpio, i, "ui.button_list", "gpio");
  config_.read(&button_name, i, "ui.button_list", "name");

  // do something with these values ...
}
```


write
-----------
write will create a new key if the key does not exist in the database.
writing to a key uses the same setup as read: 

```
#include "adk/config/config.h"

std::unique_ptr<Config> config_ = adk::config::Config::Create(CORE_CONFIG_FILENAME);
if (config_ == nullptr) {
  return false;
}
int pwr_pio_value = 10;
bool status;
status = config_.Write(pwr_pio_value,"key");
```

restore 
-----------

restore can be called on a key to restore the value to value_default:
```
#include "adk/config/config.h"

std::unique_ptr<Config> config_ = adk::config::Config::Create(CORE_CONFIG_FILENAME);
if (config_ == nullptr) {
  return false;
}
bool status;
status = config_.Restore("key");
```

meta
-----------

TODO - add or edit meta information

Constraint JSON format
=============================
The constraints in the meta information is stored using JSON format.
There are three types of constraint. `range`, `list` and `enum` a constraint can constain multiple ranges. i.e 1-10 and 30-50
A range **shall** have a min and a max key.

example list json format with strings:

`{"list":["string1","string2","string3"]}`

example list with ints:

`{"list":[1,2,3]}`

example single range:

`{"range":[{"min":1,"max":10}]}`

example multi range:

`{"range":[{"min":1,"max":10},{"min":30,"max":50}]}`


example multi range:

`{"enum":{"FAILURE":1,"WARNING":2,"SUCCESS":3}}`


ADKCFG Database Schema
=============================

The v1.0 of the adkcfg schema for any database files can be found here at: [`docs/adk-schema-v1.0.sql`](docs/adk-schema-v1.0.sql)
Any modification to this schema should increment the database_version number. 

To create an empty database using the sqlite binary the following command can be used:
```
cat adk-schema-v1.0.sql | sqlite3 empty.db
```

Release Notes
=============================
* Since 2018-03-08 all release comments are provided in the git change history

* 2018-03-08
  * added .write(). Will create a new key if the key doesn't already exist

* 2018-02-21
  * Library won't crash if the key doesn't exist, but it won't detect that it's missing either.
    * Reading a non-existent string will return ""
    * reading a non-existent integer will return an error.
    * reading a non-existent database will return the same as a missing key
    * todo!
  * only .read() works, no support for any other calls.

[old]
* Major reshuffle of files and CMake scripts. Now creates static and shared library versions.
* Added helper members for reading an array of values.
* First commit, requires massive cleanup, can only read a single value.
